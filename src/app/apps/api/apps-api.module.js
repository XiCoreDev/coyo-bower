(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.apps.api
   *
   * @description
   * This module provides an API for managing, registering and displaying apps.
   */
  angular
      .module('coyo.apps.api', [
        'coyo.base',
        'commons.config',
        'commons.resource'
      ])
      .run(registerErrorHandler);

  function registerErrorHandler($log, $rootScope, appRegistry, appService) {
    $rootScope.$on('$stateChangeError', function (event, toState, toParams) {
      angular.forEach(appRegistry.getAppSenderTypes(), function (senderType) {
        if (_.startsWith(toState.name, 'main.' + senderType + '.show.apps')) {
          $log.debug('[appRegistryErrorHandler] Error loading app, redirecting to sender.');
          appService.redirectToSender({typeName: senderType, slug: toParams.idOrSlug});
        }
      });
    });
  }

})(angular);
