(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.api')
      .factory('appChooserModalService', appChooserModalService)
      .controller('AppChooserModalController', AppChooserModalController);

  /**
   * @ngdoc service
   * @name coyo.apps.api.appChooserModalService
   *
   * @description
   * This service opens a modal in which the user can choose and configure a new app. The modal consists of two
   * pages. One for selecting the type of the new app and one for specifying the settings of the chosen app type.
   *
   * @requires $uibModalInstance
   * @requires msmModal
   * @requires sender
   */
  function appChooserModalService(msmModal) {

    return {
      open: open
    };

    /**
     * @ngdoc method
     * @name coyo.apps.api.appChooserModalService#open
     * @methodOf coyo.apps.api.appChooserModalService
     *
     * @description
     * Opens the modal to choose and configure an app from.
     *
     * @param {object} sender
     * The sender context the newly created app should belong to.
     *
     * @returns {object}
     * Returns a promise with the saved app.
     */
    function open(sender) {
      return msmModal.open({
        size: 'lg',
        templateUrl: 'app/apps/api/components/app-chooser/app-chooser-modal.html',
        controller: 'AppChooserModalController',
        resolve: {
          sender: function () { return sender; }
        }
      }).result;
    }
  }

  function AppChooserModalController($q, $rootScope, $uibModalInstance, sender) {
    var vm = this;

    vm.senderType = sender.typeName;
    vm.saveCallbacks = {
      onBeforeSave: function () {
        return $q.resolve();
      }
    };

    vm.goBack = goBack;
    vm.save = save;

    function goBack() {
      vm.selectedApp = null;
    }

    function save() {
      vm.selectedApp.senderId = sender.id;
      return vm.saveCallbacks.onBeforeSave().then(function () {
        return vm.selectedApp.createWithPermissions(['manage']).then(function (app) {
          $rootScope.$emit('app:created', app);
          $uibModalInstance.close(app);
        });
      });
    }
  }

})(angular);
