(function (angular) {
  'use strict';

  angular.module('coyo.apps.api')
      .factory('appSettingsModalService', appSettingsModalService)
      .controller('AppSettingsModalController', AppSettingsModalController);

  /**
   * @ngdoc service
   * @name coyo.apps.api.appSettingsModalService
   *
   * @description
   * This service provides a method to open a modal dialog which uses
   * {@link coyo.apps.api.oyocAppSettings:oyocAppSettings appSettings} and
   * {@link commons.ui.oyocSettingsView:oyocSettingsView settingsView} to display the settings
   * of a passed app. Within this settings a user can edit the default settings (like 'name' and 'active'), all custom
   * settings and can delete the app.
   *
   * @requires appService
   * @requires msmModal
   */
  function appSettingsModalService(appService, msmModal) {

    return {
      open: open
    };

    /**
     * @ngdoc method
     * @name coyo.apps.api.appSettingsModalService#open
     * @methodOf coyo.apps.api.appSettingsModalService
     *
     * @description
     * This method opens the modal which shows the settings of the passed app. Note that this service reloads the
     * active app, when the app itself changed.
     *
     * @param {object} app
     * The app to show the settings for. Note that the app gets copied, when opening the modal and
     * the modified copy is returned on success.
     *
     * @returns {object} A promise with the updated (or deleted) app. If the app got deleted a property "deleted" with
     * the value "true" is added to the returned object.
     */
    function open(app) {
      return msmModal.open({
        size: 'lg',
        templateUrl: 'app/apps/api/components/app-settings/app-settings-modal.html',
        controller: 'AppSettingsModalController',
        resolve: {
          app: function () {
            return angular.copy(app);
          }
        }
      }).result.then(function (app) {
        if (!app.deleted) {
          appService.reloadApp(app);
        }
        return app;
      });
    }
  }

  function AppSettingsModalController(app, $q, $log, $uibModalInstance, $rootScope, SenderModel) {
    var vm = this;

    vm.app = app;
    vm.saveCallbacks = {
      onBeforeSave: function () {
        return $q.resolve();
      }
    };

    vm.save = save;
    vm.remove = remove;

    function save() {
      $log.debug('[AppSettingsModalController] Saving app', vm.app);
      vm.saveCallbacks.onBeforeSave().then(function () {
        return vm.app.save().then(function (app) {
          $rootScope.$emit('app:updated', app);
          $uibModalInstance.close(app);
        });
      });
    }

    function remove() {
      $log.debug('[AppSettingsModalController] Removing app', vm.app);
      var senderModel = new SenderModel({id: app.senderId});
      return senderModel.removeApp(app.id).then(function (app) {
        app.deleted = true;
        $rootScope.$emit('app:deleted', app.id);
        $uibModalInstance.close(app);
      });
    }
  }

})(angular);
