(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.blog')
      .controller('BlogArticleEditController', BlogArticleEditController);

  /**
   * Controller for creating and editing a blog article.
   *
   * @requires $scope
   * @requires $state
   * @requires $timeout
   * @requires widgetLayoutService
   * @requires article
   * @requires app
   * @requires sender
   * @constructor
   */
  function BlogArticleEditController($scope, $state, $timeout, widgetLayoutService, article, app, sender) {
    var vm = this;
    vm.article = article;
    vm.originalArticle = angular.copy(vm.article);
    vm.app = app;
    vm.sender = sender;
    vm.editMode = true;
    vm.activeTab = 0;
    vm.article.publishAsAuthor = angular.isUndefined(vm.article.publishAsAuthor) ? true : vm.article.publishAsAuthor;
    vm.raiseNotification = true;
    vm.teaserImage = article.teaserImage;
    vm.teaserImageWide = article.teaserImageWide;
    vm.simpleMode = true;

    /**
     * Wanted behaviour:
     * - User is publisher and creates a new article: Instant publishing
     * - User is publisher/editor and edits a draft: No instant publishing
     * - User is not publisher: No publishing possible
     */
    if (vm.article.publishDate) {
      vm.publishStatus = 'PUBLISHED_AT';
      vm.article.publishDate = new Date(vm.article.publishDate);
    } else if (vm.app._permissions.publishArticle && vm.article.isNew()) {
      vm.publishStatus = 'PUBLISHED';
    } else {
      vm.publishStatus = 'DRAFT';
    }

    vm.save = save;
    vm.cancel = cancel;

    function save() {
      vm.article.teaserImage = vm.teaserImage ? _.pick(vm.teaserImage, ['fileId', 'senderId']) : null;
      vm.article.teaserImageWide = vm.teaserImageWide ? _.pick(vm.teaserImageWide, ['fileId', 'senderId']) : null;

      if (vm.publishStatus === 'DRAFT') {
        vm.article.publishDate = null;
      } else if (vm.publishStatus === 'PUBLISHED' || !vm.article.publishDate) {
        vm.article.publishDate = new Date();
      }

      return article.save(vm.raiseNotification).then(function () {
        $timeout(function () { // wait for article ID to be synced to the widget layout and slots
          widgetLayoutService.save($scope).then(function () {
            $state.go('^.view', {id: vm.article.id});
          });
        });
      });
    }

    function cancel() {
      widgetLayoutService.cancel($scope);
      if (article.isNew()) {
        $state.go('^');
      } else {
        $state.go('^.view', {id: vm.article.id});
      }
    }

    /* ===== PRIVATE METHODS ===== */

    (function _init() {
      widgetLayoutService.onload($scope).then(function () {
        widgetLayoutService.edit($scope);
      });
    })();
  }

})(angular);
