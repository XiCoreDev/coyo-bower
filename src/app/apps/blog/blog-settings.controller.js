(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.blog')
      .controller('BlogSettingsController', BlogSettingsController);

  function BlogSettingsController($scope) {
    var vm = this;

    vm.app = $scope.model;
    vm.app.settings.authorType = _.getNullUndefined(vm, 'app.settings.authorType', 'VIEWER');
    vm.app.settings.publisherType = _.getNullUndefined(vm, 'app.settings.publisherType', 'VIEWER');
    vm.app.settings.commentsAllowed = _.getNullUndefined(vm, 'app.settings.commentsAllowed', true);
  }

})(angular);
