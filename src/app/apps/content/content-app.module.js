(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.apps.content
   *
   * @description
   * # Content app module #
   * The content app module contains the content app objects.
   *
   * @requires coyo.apps.api.appRegistryProvider
   */
  angular
      .module('coyo.apps.content', [
        'coyo.base',
        'coyo.apps.api',
        'commons.config',
        'commons.ui',
        'commons.i18n'
      ])
      .config(registerApp);

  function registerApp(appRegistryProvider) {
    appRegistryProvider.register({
      name: 'APP.CONTENT.NAME',
      description: 'APP.CONTENT.DESCRIPTION',
      key: 'content',
      icon: 'zmdi-view-compact',
      states: [
        {
          params: {created: null},
          templateUrl: 'app/apps/content/content-main.html',
          controller: 'ContentAppMainController',
          controllerAs: '$ctrl',
          resolve: {
            user: /*@ngInject*/ function (authService) {
              return authService.getUser();
            }
          }
        }
      ]
    });
  }

})(angular);
