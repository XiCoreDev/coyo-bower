(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.content')
      .controller('ContentAppMainController', ContentAppMainController);

  function ContentAppMainController($scope, app, widgetLayoutService, $stateParams) {
    var vm = this;
    vm.app = app;
    vm.editMode = false;

    vm.save = save;
    vm.cancel = cancel;
    vm.edit = edit;

    _init();

    ////////////////////////////////////////////////////////////////

    function edit() {
      widgetLayoutService.edit($scope);
      vm.editMode = true;
    }

    function save() {
      vm.saving = true;

      widgetLayoutService.save($scope).then(function () {
        vm.editMode = false;
      }).catch(function () {
        edit();
      }).finally(function () {
        vm.saving = false;
      });
    }

    function cancel() {
      widgetLayoutService.cancel($scope);
      vm.editMode = false;
    }

    function _init() {
      if ($stateParams.created) {
        widgetLayoutService.onload($scope).then(function () {
          edit();
        });
      }
    }
  }

})(angular);
