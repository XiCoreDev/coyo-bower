(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.file-library')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "APP.FILE_LIBRARY.DESCRIPTION": "Lade Dateien in die Dokumenten-App hoch, sortiere sie in Ordnern und teile sie mit deinen Kollegen.",
          "APP.FILE_LIBRARY.NAME": "Dokumente",
          "APP.FILE_LIBRARY.SETTINGS.ADVANCED": "Erweiterte Einstellungen",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.APP_LOCKED": "Der Ordner wird bereits in einer anderen App benutzt. Bitte wähle einen anderen Ordner aus.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.EMPTY.LABEL": "Neuen Ordner erstellen",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.EMPTY.DESCRIPTION": "Lege automatisch einen leeren Ordner an.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.LABEL": "Basis-Ordner",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.REQUIRED": "Diese Angabe ist notwendig. Bitte wähle einen Order aus.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.SELECT.LABEL": "Bestehenden Ordner auswählen",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.SELECT.DESCRIPTION": "Nutze einen bestehenden Ordner dieser Seite oder dieses Workspaces.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.PICKER_TITLE": "Wähle einen Basis-Ordner",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.LABEL": "Berechtigungen",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.EVERYONE.LABEL": "Alle",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.EVERYONE.DESCRIPTION": "Alle Benutzer können Dokumente hochladen und bearbeiten.",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.ADMINS.LABEL": "Admins",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.ADMINS.DESCRIPTION": "Nur Admins können Dokumente hochladen und bearbeiten."
        });
        /* eslint-enable quotes */
      });
})(angular);
