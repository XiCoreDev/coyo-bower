(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.file-library')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "APP.FILE_LIBRARY.DESCRIPTION": "Upload files to the documents app, sort them into folders and share them with your colleagues",
          "APP.FILE_LIBRARY.NAME": "Documents",
          "APP.FILE_LIBRARY.SETTINGS.ADVANCED": "Advanced Settings",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.APP_LOCKED": "The folder is already used by another app. Please select a different folder.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.EMPTY.LABEL": "Create new folder",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.EMPTY.DESCRIPTION": "Automatically create an empty folder.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.LABEL": "Base Folder",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.REQUIRED": "This is mandatory. Please select a folder.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.SELECT.LABEL": "Select existing folder",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.SELECT.DESCRIPTION": "Select an existing folder of this page or workspace.",
          "APP.FILE_LIBRARY.SETTINGS.FOLDER.PICKER_TITLE": "Select a Base Folder",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.LABEL": "Editors",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.EVERYONE.LABEL": "Everyone",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.EVERYONE.DESCRIPTION": "All users can upload and modify files.",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.ADMINS.LABEL": "Admins",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.ADMINS.DESCRIPTION": "Only admins can upload and modify files."
        });
        /* eslint-enable quotes */
      });
})(angular);
