(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.file-library')
      .controller('FileLibraryAppSettingsController', FileLibraryAppSettingsController);

  function FileLibraryAppSettingsController($q, $log, $scope, SenderModel, FolderModel) {
    var vm = this;

    vm.app = $scope.model;
    vm.isNewApp = (!vm.app.id);
    vm.options = {
      senderAsRoot: true,
      selectMode: 'folder'
    };

    vm.app.settings.modifyRole = vm.app.settings.modifyRole || 'NONE';
    vm.folderMode = (vm.isNewApp) ? 'CREATE' : '';
    vm.showAdvanced = (!vm.isNewApp && !vm.app.settings.folder);

    SenderModel.getWithPermissions(SenderModel.getCurrentIdOrSlug(), null, ['manage', 'createFile'])
        .then(function (sender) {
          vm.sender = sender;
          vm.app.senderId = sender.id;

          $scope.saveCallbacks.onBeforeSave = function () {
            if (vm.isNewApp && !vm.app.settings.folder) {
              $log.debug('[DocumentsApp] No folder selected. Creating default for this app.');
              var newFolder = new FolderModel({senderId: sender.id});
              newFolder.folder = true;
              newFolder.name = vm.app.name;
              return newFolder.save().then(function (folder) {
                vm.app.settings.folder = folder;
              });
            }
            return $q.resolve();
          };
        });
  }

})(angular);

