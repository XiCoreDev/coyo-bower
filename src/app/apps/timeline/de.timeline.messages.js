(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.timeline')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "APP.TIMELINE.DESCRIPTION": "Die Timeline-App zeigt die Timeline der aktuellen Seite bzw. des aktuellen Workspaces an.",
          "APP.TIMELINE.NAME": "Timeline"
        });
        /* eslint-enable quotes */
      });
})(angular);
