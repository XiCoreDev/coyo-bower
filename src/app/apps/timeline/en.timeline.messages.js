(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.timeline')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "APP.TIMELINE.DESCRIPTION": "The timeline app displays the timeline of the current page or workspace.",
          "APP.TIMELINE.NAME": "Timeline"
        });
        /* eslint-enable quotes */
      });
})(angular);
