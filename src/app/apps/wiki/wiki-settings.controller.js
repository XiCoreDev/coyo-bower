(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.wiki')
      .controller('WikiSettingsController', WikiSettingsController);

  function WikiSettingsController($scope) {
    var vm = this;

    vm.app = $scope.model;
    vm.app.settings.editorType = _.getNullUndefined(vm, 'app.settings.editorType', 'VIEWER');
    vm.app.settings.commentsAllowed = _.getNullUndefined(vm, 'app.settings.commentsAllowed', false);
  }

})(angular);
