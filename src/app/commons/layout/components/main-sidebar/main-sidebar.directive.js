(function (angular) {
  'use strict';

  angular
      .module('commons.layout')
      .config(registerToCurtain)
      .directive('oyocMainSidebar', mainSidebar)
      .controller('MainSidebarController', MainSidebarController);

  function registerToCurtain(curtainServiceProvider) {
    curtainServiceProvider.register('mainSidebar');
  }

  /**
   * @ngdoc directive
   * @name commons.layout.oyocMainSidebar:oyocMainSidebar
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Displays the sidebar navigation menu (only on mobile devices). The sidebar is registered to the sidebarService by
   * registering with its API.
   *
   * @requires $rootScope
   * @requires $state
   * @requires commons.ui.userGuideService
   * @requires commons.ui.sidebarService
   * @requires coyo.admin.adminStates
   * @requires commons.auth.authService
   * @requires commons.ui.curtainService
   */
  function mainSidebar() {
    return {
      restrict: 'E',
      templateUrl: 'app/commons/layout/components/main-sidebar/main-sidebar.html',
      replace: true,
      scope: {},
      bindToController: {
        landingPages: '<'
      },
      controller: 'MainSidebarController',
      controllerAs: '$ctrl'
    };
  }

  function MainSidebarController($rootScope, $state, userGuideService, sidebarService, adminStates, authService,
                                 curtainService, tourService, termsService, UserNotificationSettingModel) {
    var vm = this;
    vm.showSidebar = false;
    vm.allAdminPermissions = _.map(adminStates, 'globalPermission').join(',');

    vm.open = open;
    vm.close = close;
    vm.help = help;
    vm.restartTour = restartTour;
    vm.hasTourSteps = hasTourSteps;
    vm.logout = authService.logout;
    vm.showTerms = termsService.showModal;

    // ----------------------------------------------------------------

    function open() {
      vm.showSidebar = true;
      $rootScope.showBackdrop = true;
    }

    function close() {
      vm.showSidebar = false;
      $rootScope.showBackdrop = false;
    }

    function help() {
      close();

      if ($state.current && $state.current.data && $state.current.data.guide) {
        userGuideService.open($state.current.data.guide);
      } else {
        userGuideService.notFound();
      }
    }

    function hasTourSteps() {
      return tourService.getTopics().length > 0;
    }

    function restartTour() {
      close();
      tourService.restart('mobile');
    }

    // ----------------------------------------------------------------

    function _checkNotificationSettings() {
      UserNotificationSettingModel.query({}, {userId: vm.user.id}).then(function (result) {
        vm.hasNotificationSettings = result.length > 0;
      });
    }

    function _checkTermsActive() {
      termsService.termsActive().then(function (active) {
        vm.termsActive = active;
      });
    }

    (function _init() {
      sidebarService.register({
        name: 'menu',
        open: open,
        close: close
      });

      authService.getUser().then(function (user) {
        vm.user = user;
        _checkTermsActive();
        _checkNotificationSettings();
      }).finally(function () {
        curtainService.loaded('mainSidebar');
      });
    })();
  }

})(angular);
