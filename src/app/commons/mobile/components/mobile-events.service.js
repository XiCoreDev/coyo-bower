(function () {
  'use strict';

  angular.module('commons.mobile')
    .factory('mobileEventsService', mobileEventsService);

  /**
   * @ngdoc service
   * @name commons.mobile.mobileEventsService
   *
   * @description
   * Sends client events to native mobile apps. At the moment iOS and Android are supported. For iOS the events are
   * propagated to the $window object. iOS attaches a message handler to this object which is used. For Android the
   * a custom interface which is injected by the app is created / used.
   *
   * See: http://www.joshuakehn.com/2014/10/29/using-javascript-with-wkwebview-in-ios-8.html
   *
   * @requires $window
   * @requires $log
   */
  function mobileEventsService($window, $log) {

    return {
      propagate: propagate
    };

    /**
     * @ngdoc method
     * @name commons.mobile.mobileEventsService#propagate
     * @methodOf commons.mobile.mobileEventsService
     *
     * @description
     * Propagate an event with the given name and optionally a payload to a native app which might call this application
     * from within a webview. Only if this is the case an event is propagated. The send event is wrapped in an object
     * containing the name and the payload.
     *
     * @param {string} eventName a unique descriptor of the event. It is convention to separate tokens of the events
     * name by colons, e.g. user:updated:success.
     * @param {object=} payload optional payload to send along with the event.
     */
    function propagate(eventName, payload) {
      /* eslint-disable no-undef, angular/definedundefined */
      if ($window.webkit || typeof AndroidJavascriptBridge !== 'undefined') {
        var eventData = {name: eventName};
        if (payload) {
          eventData.data = payload;
        }

        if ($window.webkit) {
          $log.info('[authService] Propagating event to iOS app.', eventData);
          $window.webkit.messageHandlers.coyo.postMessage(eventData);
        }

        if (typeof AndroidJavascriptBridge !== 'undefined') {
          $log.info('[authService] Propagating event to android app.', eventData);
          AndroidJavascriptBridge.postMessage(angular.toJson(eventData, false));
        }
      }
      /* eslint-enable no-undef, angular/definedundefined */
    }

  }
})();
