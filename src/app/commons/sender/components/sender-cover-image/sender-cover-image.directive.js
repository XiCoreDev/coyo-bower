(function () {
  'use strict';

  angular
      .module('commons.sender')
      .directive('coyoSenderCoverImage', SenderCoverImage)
      .controller('senderCoverImageController', senderCoverImageController);

  /**
   * @ngdoc directive
   * @name commons.sender.coyoSenderCoverImage:coyoSenderCoverImage
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * This directive renders the cover image for a sender. Therefore it needs the url to the cover image and then takes
   * care that the correct image for the correct device width is requested from the server and rendered. It also takes
   * into consideration whether the display supports retina or not. This directive listens to a global resize event and
   * is notified of changes in the display's width.
   *
   * @param {string} imageUrl
   * The base url for the cover image without the imageSize parameter. This parameter is determined and set by the
   * directive.
   *
   * @requires $rootScope
   * @requires $scope
   * @requires commons.resource.backendUrlService
   */
  function SenderCoverImage() {
    return {
      restrict: 'E',
      templateUrl: 'app/commons/sender/components/sender-cover-image/sender-cover-image.html',
      scope: {},
      bindToController: {
        imageUrl: '<?'
      },
      controller: 'senderCoverImageController',
      controllerAs: '$ctrl'
    };
  }

  function senderCoverImageController($rootScope, $scope, backendUrlService) {
    var vm = this;
    vm.coverImageUrl = '';

    var backendUrl = backendUrlService.getUrl();

    // ---------------------- PRIVATE METHODS ------------------------

    function _setCover(isLarge) {
      vm.coverImageUrl = (isLarge)
          ? _createCoverUrl($rootScope.screenSize.isRetina ? 'ORIGINAL' : 'XXL')
          : _createCoverUrl($rootScope.screenSize.isRetina ? 'XXL' : 'XL');
    }

    function _createCoverUrl(size) {
      return vm.imageUrl ? 'url(' + backendUrl + vm.imageUrl + '&imageSize=' + size + ')' : 'none';
    }

    // ---------------------------- INIT ----------------------------

    (function _init() {
      // change cover image if image url changed
      $scope.$watch(function () {
        return vm.imageUrl;
      }, function () {
        _setCover($rootScope.screenSize.isMd || $rootScope.screenSize.isLg);
      });

      // change cover if screen size changed
      var unsubscribe = $rootScope.$on('screenSize:changed', function (event, screenSize) {
        _setCover(screenSize.isMd || screenSize.isLg);
      });
      $scope.$on('$destroy', unsubscribe);
    })();
  }
})();
