(function (angular) {
  'use strict';

  angular
      .module('commons.shares')
      .controller('ShareModalController', shareModalController);

  function shareModalController(parentIsPublic) {
    var vm = this;
    vm.parentIsPublic = parentIsPublic;
  }

})(angular);
