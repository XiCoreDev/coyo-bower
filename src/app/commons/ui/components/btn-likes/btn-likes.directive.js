(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('coyoBtnLikes', btnLikes)
      .controller('BtnLikeController', BtnLikeController)
      .controller('BtnLikeModalController', BtnLikeModalController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoBtnLikes:coyoBtnLikes
   * @scope
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders a like button from the perspective of the current user. The button is rendered in a condensed style if the
   * directive defines a condensed attribute.
   *
   * @requires $scope
   * @requires $log
   * @requires authService
   * @requires LikeModel
   * @requires msmModal
   *
   * @param {object} target The like target.
   * @param {string} target.id The ID of the target.
   * @param {string} target.typeName The type of the target.
   * @param {string} [init] Initial like data.
   */
  function btnLikes() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: function (elem, attrs) {
        var isCondensed = angular.isDefined(attrs.condensed);
        return 'app/commons/ui/components/btn-likes/btn-likes' + (isCondensed ? '.condensed.html' : '.html');
      },
      scope: {},
      bindToController: {
        target: '<',
        init: '<'
      },
      controller: 'BtnLikeController',
      controllerAs: '$ctrl'
    };
  }

  function BtnLikeController($scope, $log, authService, msmModal, LikeModel, likesService) {
    var vm = this;

    vm.lastUpdate = new Date().getTime();
    vm.tooltipUrl = 'app/commons/ui/components/btn-likes/btn-likes.tooltip.html';
    vm.tooltipUrlCondensed = 'app/commons/ui/components/btn-likes/btn-likes.tooltip.condensed.html';

    vm.like = like;
    vm.show = show;

    function like() {
      if (vm.isLoading || !vm.currentUser || !vm.likeData) {
        return; // currently loading
      }

      vm.isLoading = true;
      var request = vm.likeData.self
          ? LikeModel.unlike(vm.target.id, vm.target.typeName, vm.currentUser.id)
          : LikeModel.like(vm.target.id, vm.target.typeName, vm.currentUser.id);
      request.then(_set).finally(function () {
        vm.isLoading = false;
        vm.lastUpdate = new Date().getTime();
      });
    }

    function show() {
      if (vm.isLoading || !vm.currentUser || !vm.likeData) {
        return; // currently loading
      }

      msmModal.open({
        templateUrl: 'app/commons/ui/components/btn-likes/btn-likes.modal.html',
        controller: 'BtnLikeModalController',
        resolve: {
          target: function () {
            return vm.target;
          }
        }
      });
    }

    function _set(likeInfo) {
      vm.likeData = {
        self: likeInfo.likedBySender,
        total: likeInfo.latest,
        totalCount: likeInfo.count,
        others: _.reject(likeInfo.latest, {id: vm.currentUser.id}),
        othersCount: likeInfo.count - likeInfo.likedBySender
      };
    }

    function _init(permission, currentUser) {
      vm.visible = permission;
      vm.currentUser = currentUser;

      // load like info
      if (vm.init) {
        _set(vm.init);
      } else {
        vm.isLoading = true;
        likesService.getLikes(vm.target.id, vm.target.typeName, vm.currentUser.id)
          .then(_set)
          .catch(function () {
            $log.error('[Likes] Could not load likes for target with id [' + vm.target.id + ']');
          })
          .finally(function () {
            vm.isLoading = false;
            vm.lastUpdate = new Date().getTime();
          });
      }
    }

    var unsubscribeFn = authService.onGlobalPermissions('LIKE', _init);
    $scope.$on('$destroy', unsubscribeFn);
  }

  function BtnLikeModalController(LikeModel, Pageable, target) {
    var vm = this;

    vm.target = target;
    vm.likes = [];
    vm.loadMore = loadMore;

    function loadMore() {
      if (!vm.currentPage || !vm.currentPage.last) {
        vm.loading = true;

        var pageable = new Pageable((vm.currentPage ? vm.currentPage.number + 1 : 0), 20, 'created,desc');
        LikeModel.pagedQuery(pageable, {}, {
          targetId: vm.target.id,
          targetType: vm.target.typeName
        }).then(function (page) {
          vm.currentPage = page;
          vm.likes = vm.likes.concat(page.content);
        }).finally(function () {
          vm.loading = false;
        });
      }
    }
  }

})(angular);
