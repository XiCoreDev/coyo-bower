(function (angular) {
  'use strict';

  /**
   * @ngdoc directive
   * @name commons.ui.coyoComment:coyoComment
   * @scope
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders a single comment
   *
   * @param {object} ngModel The comment
   */
  angular
      .module('commons.ui')
      .directive('coyoComment', comment)
      .controller('CommentController', CommentController);

  function comment() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/commons/ui/components/comment/comment.html',
      scope: {},
      require: 'ngModel',
      bindToController: {
        ngModel: '='
      },
      controller: 'CommentController',
      controllerAs: '$ctrl'
    };
  }

  function CommentController(backendUrlService, coyoEndpoints) {
    var vm = this;
    vm.previewUrl = coyoEndpoints.comments.preview;
    vm.backendUrl = backendUrlService.getUrl();
    vm.remove = remove;

    function remove() {
      vm.ngModel.delete();
    }
  }

})(angular);
