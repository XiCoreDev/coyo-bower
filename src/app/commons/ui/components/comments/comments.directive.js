(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('coyoComments', comments)
      .controller('CommentsController', CommentsController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoComments:coyoComments
   * @scope
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders comments as a panel-footer element.
   *
   * @requires $scope
   * @requires $timeout
   * @requires $log
   * @requires authService
   * @requires socketService
   * @requires CommentModel
   * @requires Pageable
   * @requires commentsService
   *
   * @param {object} target The comment target.
   * @param {string} target.id The target ID.
   * @param {string} target.typeName The target type.
   * @param {string} [init] Initial comment data.
   */
  function comments() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/commons/ui/components/comments/comments.html',
      scope: {},
      bindToController: {
        target: '<',
        init: '<'
      },
      controller: 'CommentsController',
      controllerAs: '$ctrl'
    };
  }

  function CommentsController($scope, $timeout, $log, authService, socketService, CommentModel, Pageable, tempUploadService,
                              commentsService) {
    var vm = this;

    vm.comments = [];
    vm.newComments = [];
    vm.loading = false;
    vm.total = 0;

    vm.loadMore = loadMore;
    vm.formKeyPress = formKeyPress;
    vm.isUploading = isUploading;
    vm.submitForm = submitForm;
    vm.resetForm = resetForm;
    vm.addAttachments = addAttachments;
    vm.removeAttachment = removeAttachment;

    var unsubscribeCreateFn =
        socketService.subscribe('/topic/comment.' + vm.target.id + '.' + vm.target.typeName + '.created', _add);
    var unsubscribeDeleteFn =
        socketService.subscribe('/topic/comment.' + vm.target.id + '.' + vm.target.typeName + '.deleted', _remove);
    $scope.$on('$destroy', unsubscribeCreateFn);
    $scope.$on('$destroy', unsubscribeDeleteFn);

    // ------------------------------------------------------------------------------------------------

    function loadMore() {
      if (!vm.loading) {
        vm.loading = true;

        var page = (vm.currentPage ? vm.currentPage.number + 1 : 0);
        var pageSize = (!vm.currentPage ? 2 : 8);
        var offset = vm.comments.length + vm.newComments.length;
        var pageable = new Pageable(page, pageSize, 'created,desc', offset);
        var params = {
          targetId: vm.target.id,
          targetType: vm.target.typeName
        };

        CommentModel.pagedQueryWithPermissions(pageable, params, {}, ['delete']).then(_setPage).finally(function () {
          vm.loading = false;
        });
      }
    }

    function formKeyPress($event) {
      if ($event.keyCode === 13 && !$event.ctrlKey && !$event.shiftKey) {
        $event.preventDefault();
        submitForm();
      }
    }

    function isUploading() {
      return _.filter(vm.newItemAttachments, {uploading: true}).length > 0;
    }

    function submitForm() {
      if (vm.form.$valid && !isUploading()) {
        vm.loading = true;

        vm.formModel.attachments = _.map(vm.newItemAttachments, function (attachment) {
          return {
            name: attachment.name,
            uid: attachment.uid,
            contentType: attachment.contentType
          };
        });

        return vm.formModel.create().then(resetForm);
      }

      return null;
    }

    function resetForm() {
      $timeout(function () {
        vm.loading = false;
        vm.newItemAttachments = [];

        vm.formModel = new CommentModel({
          authorId: vm.currentUser.id,
          targetId: vm.target.id,
          targetType: vm.target.typeName
        });

        if (vm.form) {
          vm.form.$setPristine();
          vm.form.$setUntouched();
        }
      });
    }

    function addAttachments(files) {
      vm.newItemAttachments.push.apply(vm.newItemAttachments, files);

      angular.forEach(files, function (file) {
        tempUploadService.upload(file, 300).then(function (blob) {
          angular.extend(file, blob);
        }).catch(function () {
          removeAttachment(file);
        });
      });

      vm.focusMessageFormField = true;
    }

    function removeAttachment(file) {
      vm.newItemAttachments = _.reject(vm.newItemAttachments, file);
      vm.focusMessageFormField = true;
    }

    // ------------------------------------------------------------------------------------------------

    function _add(event) {
      $scope.$apply(function () {
        CommentModel.getWithPermissions(event.content, {}, ['delete']).then(function (comment) {
          vm.newComments.push(new CommentModel(comment));
          vm.total++;
        });
      });
    }

    function _remove(event) {
      $scope.$apply(function () {
        _.remove(vm.comments, {id: event.content});
        _.remove(vm.newComments, {id: event.content});
        vm.total--;
      });
    }

    function _setPage(page) {
      vm.currentPage = page;
      vm.comments = _.concat(page.content.reverse(), vm.comments);
      vm.total = page.totalElements;
    }

    (function _init() {
      // load current user
      authService.getUser().then(function (user) {
        vm.currentUser = user;
        resetForm();
      });

      vm.loading = true;

      commentsService.getComments(vm.target.id, vm.target.typeName)
        .then(_setPage)
        .catch(function () {
          $log.error('[Comments] Could not load comments for target with id [' + vm.target.id + ']');
        })
        .finally(function () {
          vm.loading = false;
        });
    })();
  }

})(angular);
