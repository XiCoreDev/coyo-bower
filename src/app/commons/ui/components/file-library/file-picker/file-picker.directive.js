(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('coyoFilePicker', CoyoFilePicker)
      .controller('FilePickerController', FilePickerController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoFilePicker:coyoFilePicker
   * @scope
   * @restrict 'E'
   * @element ANY
   *
   * @description
   * Renders a button which opens the file library to select files from. The selected files are listed and can be
   * removed afterwards.
   *
   * @param {object|array} ngModel
   * An array (multi-select) or object (single/folder select) to store the selected file(s) in.
   *
   * @param {object=} sender
   * A sender can be passed along with this directive. In this case the file library is opened with this sender being
   * the active view.
   *
   * @param {string=} options
   * Sets the options for the file library. See {@link commons.ui.coyoFileLibrary:coyoFileLibrary}. The following defaults are
   * set:
   *
   * ```
   * {
   *  uploadMultiple: true,
      selectMode: 'multiple'
   * }
   * ```
   *
   * @param {object=} cropSettings
   * Defines the crop settings. See {@link commons.ui.coyoFileLibrary:coyoFileLibrary}. Disabled by default.
   *
   * @param {string=} modalTitle
   * Translation key of the title of the modal.
   */
  function CoyoFilePicker() {
    return {
      restrict: 'E',
      require: 'ngModel',
      templateUrl: 'app/commons/ui/components/file-library/file-picker/file-picker.html',
      scope: {},
      bindToController: {
        selectedFiles: '=ngModel',
        sender: '<?',
        options: '<?',
        cropSettings: '<?',
        modalTitle: '@'
      },
      controller: 'FilePickerController',
      controllerAs: '$ctrl'
    };
  }

  function FilePickerController(fileLibraryModalService, $element) {
    var vm = this;
    var ngModelController = $element.controller('ngModel');
    var defaultOptions = {
      uploadMultiple: true,
      selectMode: 'multiple'
    };

    var defaultCropSettings = {
      cropImage: false
    };

    vm.selectedFiles = vm.selectedFiles || (_.get(vm, 'options.selectMode') === 'multiple' ? [] : undefined);
    vm.options = angular.extend(defaultOptions, vm.options);
    vm.cropSettings = angular.extend(vm.cropSettings || {}, defaultCropSettings);

    vm.openFileLibrary = openFileLibrary;
    vm.removeFile = removeFile;
    vm.getSelectedFiles = getSelectedFiles;

    function openFileLibrary() {
      fileLibraryModalService.open(vm.sender, vm.options, vm.cropSettings, vm.modalTitle).then(function (selection) {
        if (vm.options.selectMode === 'multiple') {
          ngModelController.$setViewValue(_.unionWith(vm.selectedFiles, selection, function (a, b) {
            return a.id === b.id;
          }));
        } else {
          ngModelController.$setViewValue(selection);
        }
      });
    }

    function removeFile(file) {
      if (vm.options.selectMode === 'multiple') {
        ngModelController.$setViewValue(_.filter(ngModelController.$viewValue, function (f) { return f.id !== file.id; }));
      } else {
        ngModelController.$setViewValue(undefined);
      }
    }

    function getSelectedFiles() {
      if (vm.options.selectMode === 'multiple') {
        return ngModelController.$viewValue;
      }
      return ngModelController.$viewValue ? [ngModelController.$viewValue] : [];
    }
  }

})(angular);
