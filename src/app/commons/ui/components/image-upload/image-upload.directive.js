(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('coyoImageUpload', imageUpload)
      .controller('ImageUploadController', ImageUploadController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoImageUpload:coyoImageUpload
   * @scope
   * @restrict 'E'
   * @element ANY
   *
   * @description
   * Renders an image upload area.
   *
   * @param {object} ngModel model to save the image in
   * @param {boolean} selecting Boolean flag whether an image is currently selected (set by this directive)
   * @param {string} keyDragNDrop i18n message key for message to show in drop area
   * @param {string} areaType The area type of the crop tool. Options: <em>circle</em>, <em>square</em> and <em>rectangle</em>
   * @param {string} cropAspectRatio The crop aspect ratio, e.g. <em>1</em> or <em>4</em>
   * @param {object} imageSize The image size, e.g. <em>{w: 320, h: 320}</em> or <em>{w: 1600, h: 400}</em>
   */
  function imageUpload() {
    return {
      restrict: 'E',
      templateUrl: 'app/commons/ui/components/image-upload/image-upload.html',
      scope: {},
      replace: true,
      require: 'ngModel',
      bindToController: {
        ngModel: '=',
        selecting: '=',
        keyDragNDrop: '@',
        keyContentError: '@',
        areaType: '@',
        cropAspectRatio: '@',
        imageSize: '<'
      },
      controller: 'ImageUploadController',
      controllerAs: '$ctrl'
    };
  }

  function ImageUploadController() {
    var vm = this;

    vm.image = '';
    vm.ngModel = '';
    vm.contentError = false;

    vm.beforeChange = beforeChange;
    vm.onChange = onChange;

    function beforeChange() {
      vm.selecting = true;
    }

    function onChange() {
      vm.selecting = false;
      if (vm.image) {
        vm.contentError = false;
        vm.contentType = _.includes(['image/png', 'image/gif'], vm.image.type) ? 'image/png' : 'image/jpeg';
      } else {
        vm.contentError = true;
      }
    }
  }

})(angular);
