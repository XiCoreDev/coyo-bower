(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('coyoMentionEncode', mentionEncode);

  var cache = {};

  /**
   * @ngdoc directive
   * @name commons.ui.coyoMentionEncode:coyoMentionEncode
   * @restrict 'A'
   * @element ANY
   *
   * @description
   * Evaluates the expression and inserts the resulting HTML into the element in a secure way. Any mentions contained in
   * the expression are resolved asynchronously and replaced with a sender link using the sender's display name. Senders
   * are cached globally until the next successful state change.
   *
   * @requires $sce
   * @requires $parse
   * @requires $compile
   * @requires $rootScope
   * @requires $q
   * @requires coyo.domain.SenderModel
   * @requires commons.error.errorService
   * @requires $sanitize
   */
  function mentionEncode($sce, $parse, $compile, $rootScope, $q, SenderModel, errorService, $sanitize) {
    return {
      restrict: 'A',
      compile: function mentionEncodeCompile(tElem, tAttrs) {
        var mentionEncodeGetter = $parse(tAttrs.coyoMentionEncode);
        var mentionEncodeWatch = $parse(tAttrs.coyoMentionEncode, function sceValueOf(val) {
          return $sce.valueOf(val);
        });

        return function mentionEncodeLink(scope, elem) {
          scope.mentions = {};

          scope.$watch(mentionEncodeWatch, function ngBindHtmlWatchAction() {
            var value = mentionEncodeGetter(scope);

            value = value ? value.replace(/([\s|>|^])@([-0-9a-zA-Z]+)/g, function (mention, prefix, slug) {
              if (!cache[slug]) {
                cache[slug] = SenderModel.get(slug).catch(function (errorResponse) {
                  errorService.suppressNotification(errorResponse);
                  return $q.reject(errorResponse);
                });
              }

              cache[slug].then(function (result) {
                scope.mentions[slug] = result;
                var link = $compile('<a coyo-sender-link=":: mentions[\'' + slug + '\']">{{:: mentions[\'' + slug + '\'].displayName }}</a>')(scope);
                elem.find('.mention-' + slug).first().replaceWith(link);
              }).catch(function () {
                elem.find('.mention-' + slug).first().replaceWith('@' + slug);
              });

              return prefix + '<span class="mention mention-' + slug + '">@' + slug + '</span>';
            }) : value;

            var trustedHtml = $sce.getTrustedHtml(value) || '';
            elem.html($sanitize(trustedHtml));
          });

          var deregisterFn = $rootScope.$on('$stateChangeSuccess', function () {
            cache = {};
          });
          scope.$on('$destroy', deregisterFn);
        };
      }
    };
  }

})(angular);
