(function (angular) {
  'use strict';

  angular.module('commons.ui')
      .directive('coyoSelectSender', selectSender);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoSelectSender:coyoSelectSender
   * @restrict 'E'
   * @scope
   *
   * @description
   * Renders a UI select field for sender selection.
   *
   * @requires $translate
   * @requires components.ui.selectFactoryModel
   * @requires coyo.domain.SenderModel
   * @requires coyo.domain.UserSubscriptionModel
   * @requires commons.resource.Pageable
   * @requires commons.config.coyoConfig
   * @requires commons.auth.authService
   *
   * @param {object} ng-model (required) the currently selected sender
   * @param {string} placeholder (optional) translation key for the input field placeholder
   */
  function selectSender($translate, selectFactoryModel, SenderModel, UserSubscriptionModel, Pageable, coyoConfig, authService) {
    var selectDirective = selectFactoryModel({
      refresh: refresh,
      multiple: true,
      minSelectableItems: 6,
      mobile: false,
      emptyText: 'COMMONS.SELECT_SENDER.EMPTY',
      pageSize: 10,
      sublines: ['typeLabelTranslated']
    });
    selectDirective.templateUrl = 'app/commons/ui/components/select-sender/select-sender.html';

    return selectDirective;

    function refresh(pageableData, search, parameters) {
      var pageable = new Pageable(pageableData.page, pageableData.size, 'displayName.sort');
      var filters = {type: _.get(parameters, 'allowedTypeNames', ['user', 'workspace', 'page'])};
      var fields = ['displayName'];
      var aggregations = {};
      var findOnlyManagedSenders = _.get(parameters, 'findOnlyManagedSenders', false);
      var promise;
      if (search) {
        promise = SenderModel.searchWithFilter(search, pageable, filters, fields, aggregations, findOnlyManagedSenders);
      } else {
        promise = authService.getUser().then(function (user) {
          return UserSubscriptionModel.searchWithFilter(user.id, search, pageable, filters, fields, aggregations, findOnlyManagedSenders);
        });
      }

      return promise.then(function (response) {
        angular.forEach(response.content, function (item) {
          item.typeLabelTranslated = $translate.instant(coyoConfig.entityTypes[item.typeName].label);
        });

        return angular.extend(response.content, {
          meta: {
            last: response.last,
            number: response.number
          }
        });
      });
    }
  }

})(angular);
