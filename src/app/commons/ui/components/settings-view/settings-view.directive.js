(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('oyocSettingsView', SettingsView);

  /**
   * @ngdoc directive
   * @name commons.ui.oyocSettingsView:oyocSettingsView
   * @restrict E
   * @scope
   *
   * @description
   * Creates the view for the app or widget settings. These settings have to be defined when registering the app or
   * widget using the {@link coyo.apps.api.appRegistry appRegistry} or
   * {@link coyo.widgets.api.widgetRegistry widgetRegistry}. When registering an app or widget one can define a
   * template and a controller, which gets dynamically compiled and rendered by this directive.
   *
   * @param {string} model
   * The actual app to store the settings for. The model is passed into the controller's scope.
   *
   * @param {string} settings
   * The settings for the app or widget.
   *
   * @param {string} settings.templateUrl
   * An URL pointing to a template to render. This must be a valid URL and must neither be empty nor undefined.
   *
   * @param {object=} settings.controller
   * A controller for the given template if more complex logic has to be processed.
   *
   * @param {object} settingsForm
   * The settingsForm controller, needed for proper validation inside custom settings. The form controller is passed
   * into the controller's scope.
   *
   * @param {object} saveCallbacks
   * A callbacks object which can be used to modify the settings object before saving. This object is passed into the
   * controller's scope to be set with a callback method.
   *
   * @requires $compile
   */
  function SettingsView($compile) {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        model: '=',
        config: '=',
        settingsForm: '=formCtrl',
        saveCallbacks: '='
      },
      link: function (scope, elem) {
        if (scope.config.settings && scope.config.settings.templateUrl) {
          var html = '<div ng-include="\'' + scope.config.settings.templateUrl + '\'"';
          if (scope.config.settings.controller) {
            var controller = scope.config.settings.controller;
            if (scope.config.settings.controllerAs) {
              controller += ' as ' + scope.config.settings.controllerAs;
            }
            html += 'ng-controller="' + controller + '"';
          }
          html += '></div>';
          elem.append($compile(html)(scope));
        } else {
          var htmlEmpty = '';
          elem.append($compile(htmlEmpty)(scope));
        }
      }
    };
  }

})(angular);
