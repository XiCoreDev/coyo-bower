(function () {
  'use strict';

  angular
      .module('commons.ui')
      .directive('oyocSuperadminBar', superadminBar)
      .controller('superadminBarController', superadminBarController);

  /**
   * @ngdoc directive
   * @name commons.ui.superadminBar:superadminBar
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Displays a bar on the bottom of the screen when a user changes to superadmin mode. The superadmin mode can be
   * disabled with a link on the bar. Note that disabling the superadmin mode causes a full page reload to happen.
   *
   * @requires $window
   * @requires commons.auth.authService
   */
  function superadminBar() {
    return {
      restrict: 'E',
      templateUrl: 'app/commons/ui/components/superadmin-bar/superadmin-bar.html',
      scope: {},
      bindToController: {},
      controller: 'superadminBarController',
      controllerAs: '$ctrl'
    };
  }

  function superadminBarController($window, $sessionStorage, $scope, authService) {
    var vm = this;
    vm.deactivate = deactivate;

    $scope.$watch(function () {
      return _.get($sessionStorage, 'messagingSidebar.compact', false);
    }, function (newVal) {
      vm.compact = newVal;
    });

    function deactivate() {
      if (vm.user) {
        vm.user.setSuperadminMode(false).then(function () {
          $window.location.reload();
        });
      }
    }

    (function _init() {
      authService.getUser().then(function (user) {
        vm.user = user;
      });
    })();
  }
})();
