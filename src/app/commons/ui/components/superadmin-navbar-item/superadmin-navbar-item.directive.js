(function () {
  'use strict';

  angular
      .module('commons.ui')
      .directive('oyocSuperadminNavbarItem', superadminNavbarItem)
      .controller('SuperadminNavbarItemController', SuperadminNavbarItemController);

  /**
   * @ngdoc directive
   * @name commons.ui.superadminNavbarItem:superadminNavbarItem
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Displays a navigation item for users (who are superadmin) to toggle the superadmin mode on or off. Note that
   * enabling or disabling the superadmin mode causes a full page reload to happen.
   *
   * @param {object} user
   * The user to toggle the superadmin mode for.
   *
   * @requires $window
   *
   */
  function superadminNavbarItem() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/commons/ui/components/superadmin-navbar-item/superadmin-navbar-item.html',
      scope: {},
      bindToController: {
        user: '<'
      },
      controller: 'SuperadminNavbarItemController',
      controllerAs: '$ctrl'
    };
  }

  function SuperadminNavbarItemController($window) {
    var vm = this;

    vm.toggleSuperadminMode = toggleSuperadminMode;

    function toggleSuperadminMode() {
      vm.user.setSuperadminMode(!vm.user.superadminMode).then(function () {
        $window.location.reload();
      });
    }
  }
})();
