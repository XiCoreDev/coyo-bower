(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('oyocUserChooserItem', userChooserItem);

  /**
   * @ngdoc directive
   * @name coyo.profile.oyocUserChooserItem:oyocUserChooserItem
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders a list item for a user or group in the user chooser.
   *
   * @param {object} item A user or user group.
   * @param {string} itemType The item type, either "item" or "group".
   * @param {boolean} selected 'true' if the item is currently selected.
   */
  function userChooserItem() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/commons/ui/components/user-chooser-item/user-chooser-item.html',
      scope: {
        item: '<',
        itemType: '@',
        selectionType: '<?'
      }
    };
  }

})(angular);
