(function (angular) {
  'use strict';

  angular.module('commons.ui')
      .directive('coyoUserChooser', userChooser)
      .controller('UserChooserController', UserChooserController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoUserChooser:coyoUserChooser
   * @restrict 'E'
   * @element OWN
   * @scope
   *
   * @description
   * Displays a button which opens a user chooser modal via the {@link commons.ui.userChooserModalService} modal on
   * click. Within this modal users and/or groups can be selected. The button acts as a form field with it's own
   * `ngModel`.
   *
   * @param {object} ngModel
   * This directive requires an ngModel to store the selected users/groups.
   *
   * @param {boolean} loading
   * A flag which implies whether a spinner should be displayed or not.
   *
   * @param {string} [btnTitle=USER_CHOOSER.BUTTON_TITLE.SELECT]
   * The message key for the button title.
   *
   * @param {boolean} [usersOnly=false]
   * Set this parameter to only select users in the chooser.
   *
   * @param {boolean} [groupsOnly=false]
   * Set this parameter to only select groups in the chooser.
   *
   * @param {string} [usersField=userIds|users]
   * The field name to store the selected users/userIDs in.
   *
   * @param {string} [groupsField=groupIds|groups]
   * The field name to store the selected groups/groupIDs in.
   *
   * @requires commons.ui.userChooserModalService
   */
  function userChooser() {
    return {
      templateUrl: 'app/commons/ui/components/user-chooser/user-chooser.html',
      replace: true,
      restrict: 'E',
      scope: {},
      require: 'ngModel',
      bindToController: {
        ngModel: '=',
        loading: '=',
        btnTitle: '@?',
        usersOnly: '<?',
        groupsOnly: '<?',
        usersField: '@?',
        groupsField: '@?'
      },
      controller: 'UserChooserController',
      controllerAs: 'uchooser',
      link: function (scope, element, attrs, ctrl) {
        if (angular.isDefined(attrs.min) || attrs.ngMin) {
          _.set(ctrl, '$options.allowInvalid', true);

          var minVal;
          var usersField = scope.uchooser.usersField || 'userIds';
          var groupsField = scope.uchooser.groupsField || 'groupIds';

          ctrl.$validators.min = function (value) {
            if (scope.uchooser.usersOnly === true) {
              return angular.isArray(value[usersField]) && value[usersField].length >= minVal;
            } else if (scope.uchooser.groupsOnly === true) {
              return angular.isArray(value[groupsField]) && value[groupsField].length >= minVal;
            } else {
              return angular.isArray(value[usersField]) && value[usersField].length >= minVal
                  || angular.isArray(value[groupsField]) && value[groupsField].length >= minVal;
            }
          };

          attrs.$observe('min', function (value) {
            minVal = parseInt(value);
            ctrl.$validate();
          });

          scope.$watchGroup([function () {
            return _.get(ctrl.$modelValue, usersField);
          }, function () {
            return _.get(ctrl.$modelValue, groupsField);
          }], function (newVals, oldVals) {
            if (newVals !== oldVals) {
              ctrl.$validate();
            }
          });

        }
      }
    };
  }

  function UserChooserController(userChooserModalService) {
    var vm = this;

    vm.btnTitle = vm.btnTitle || 'USER_CHOOSER.BUTTON_TITLE.SELECT';
    vm.settings = {
      usersOnly: vm.usersOnly === true,
      groupsOnly: vm.groupsOnly === true,
      usersField: vm.usersField || 'userIds',
      groupsField: vm.groupsField || 'groupIds'
    };

    if (!vm.ngModel) {
      vm.ngModel = {};
      if (!vm.settings.groupsOnly) {
        vm.ngModel[vm.settings.usersField] = [];
      }
      if (!vm.settings.usersOnly) {
        vm.ngModel[vm.settings.groupsField] = [];
      }
    }

    vm.openChooser = openChooser;

    function openChooser() {
      if (!vm.loading) {
        userChooserModalService.open(vm.ngModel, vm.settings).then(function (selection) {
          angular.extend(vm.ngModel, selection);
        });
      }
    }

    function _assert(condition, message) {
      if (!condition) {
        throw new Error('[UserChooser] ' + message);
      }
    }

    /**
     * Initialize controller and check ngModel
     */
    (function _init() {
      _assert(vm.usersOnly !== true || vm.groupsOnly !== true, '"usersOnly" and "groupsOnly" must not both be set.');
      _assert(angular.isObject(vm.ngModel) && !angular.isArray(vm.ngModel), 'ngModel must be an object.');
      if (vm.ngModel[vm.settings.usersField]) {
        _assert(angular.isArray(vm.ngModel[vm.settings.usersField]), 'ngModel.' + vm.settings.usersField + ' must be an array.');
        _assert(_.every(vm.ngModel[vm.settings.usersField], angular.isString),
            'ngModel.' + vm.settings.usersField + ' must be an array of strings');
      }
      if (vm.ngModel[vm.settings.groupsField]) {
        _assert(angular.isArray(vm.ngModel[vm.settings.groupsField]), 'ngModel.' + vm.settings.groupsField + ' must be an array.');
        _assert(_.every(vm.ngModel[vm.settings.groupsField], angular.isString),
            'ngModel.' + vm.settings.groupsField + ' must be an array of strings');
      }
    })();
  }

})(angular);
