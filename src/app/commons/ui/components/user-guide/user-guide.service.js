(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .factory('userGuideService', userGuideService);

  /**
   * @ngdoc service
   * @name commons.ui.userGuideService
   *
   * @description
   * Service for rendering user guides.
   *
   * @requires msmModal
   * @requires commons.resource.backendUrlService
   * @requires $q
   * @requires $http
   */
  function userGuideService(msmModal, backendUrlService, $q, $http) {

    var guides;

    return {
      getGuideDefinition: getGuideDefinition,
      open: open,
      notFound: notFound
    };

    /**
     * @ngdoc function
     * @name commons.ui.userGuideService#getGuideDefinition
     * @methodOf commons.ui.userGuideService
     *
     * @description
     * Loads the guide definition.
     *
     * @param {string} guideKey
     * Key of the guide that should be loaded. Should be in the format category/key, e.g. 'user/markdown'.
     *
     * @return {object} Promise resolving to the guide definition tree.
     */
    function getGuideDefinition(guideKey) {
      var deferred = $q.defer();

      var category = guideKey.split('/')[0];
      var key = guideKey.split('/')[1];

      if (guides) {
        deferred.resolve(guides[category][key]);
      } else {
        $http({
          method: 'GET',
          url: backendUrlService.getUrl() + '/docs/guides/guides.json',
          autoHandleErrors: false
        }).then(function (response) {
          guides = response.data;
          deferred.resolve(guides[category][key]);
        }).catch(function (e) {
          deferred.reject(e);
        });
      }

      return deferred.promise;
    }

    /**
     * @ngdoc function
     * @name commons.ui.userGuideService#open
     * @methodOf commons.ui.userGuideService
     *
     * @description
     * Open a modal with the given user guide.
     *
     * @param {string} guideKey
     * Key of the guide that should be loaded. Should be in the format category/key, e.g. 'user/markdown'.
     */
    function open(guideKey) {
      msmModal.open({
        controller: 'UserGuideController',
        size: 'lg',
        templateUrl: 'app/commons/ui/components/user-guide/user-guide.modal.html',
        resolve: {
          key: function () {
            return guideKey;
          }
        }
      });
    }

    /**
     * @ngdoc function
     * @name commons.ui.userGuideService#notFound
     * @methodOf commons.ui.userGuideService
     *
     * @description
     * Open a modal informing that a guide was not found.
     */
    function notFound() {
      msmModal.open({
        templateUrl: 'app/commons/ui/components/user-guide/user-guide.modal.html',
        controller: function () {
          var vm = this;
          vm.notFound = true;
        }
      });
    }
  }

})(angular);
