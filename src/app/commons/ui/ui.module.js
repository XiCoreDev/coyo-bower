(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name commons.ui
   *
   * @description
   * # User interface module #
   * The user interface module contains many Coyo-specific UI components.
   *
   * If the components are not Coyo-specific, they will most likely be located in the mindsmash UI kit [1].
   *
   * <sub>
   *   [1] {@link https://github.com/mindsmash/mindsmash-ui}
   * </sub>
   */
  angular
      .module('commons.ui', [
        'coyo.base',
        'commons.auth',
        'commons.sockets',
        'commons.config',
        'commons.target',
        'commons.i18n',
        'commons.error',
        'commons.shares',
        'ksSwiper'
      ])
      .constant('marked', marked); // eslint-disable-line no-undef

})(angular);
