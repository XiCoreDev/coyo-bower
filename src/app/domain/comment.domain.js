(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('CommentModel', CommentModel);

  /**
   * @ngdoc service
   * @name coyo.domain.CommentModel
   *
   * @description
   * Provides the Coyo comment model.
   *
   * @requires restResourceFactory
   * @requires commons.config.coyoEndpoints
   */
  function CommentModel(restResourceFactory, coyoEndpoints) {
    var CommentModel = restResourceFactory({
      url: coyoEndpoints.comments.comments,
      name: 'comment'
    });

    // class members
    angular.extend(CommentModel, {

      /**
       * @ngdoc function
       * @name coyo.domain.CommentModel#getInfo
       * @methodOf coyo.domain.CommentModel
       *
       * @description
       * Gets the general comment information about this target.
       *
       * @params {string} targetId The target being liked
       * @params {string} targetType The typeName of the target being liked
       * @params {string} senderId The context sender (from whose perspective to get the info)
       *
       * @returns {promise} A $http promise
       */
      getInfo: function (targetId, targetType) {
        return CommentModel.$http({
          method: 'GET',
          url: coyoEndpoints.comments.count,
          params: {
            targetId: targetId,
            targetType: targetType
          }
        });
      }
    });

    return CommentModel;
  }
})(angular);
