(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('SenderModel', SenderModel);

  /**
   * @ngdoc service
   * @name coyo.domain.SenderModel
   *
   * @description
   * Domain model representation of sender endpoint.
   *
   * @requires restResourceFactory
   * @requires restSerializer
   * @requires coyoEndpoints
   */
  function SenderModel(restResourceFactory, $httpParamSerializer, coyoEndpoints, AppModel, $state, $stateParams, Page) {
    var SenderModel = restResourceFactory({
      url: coyoEndpoints.sender.senders
    });

    // class members
    angular.extend(SenderModel, {

      /**
       * @ngdoc method
       * @name coyo.domain.SenderModel#getCurrentIdOrSlug
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Get the ID or slug of the current sender (based on the current state)
       *
       * @returns {string} ID or slug
       */
      getCurrentIdOrSlug: function () {
        var senderParam = _.get($state.current, 'data.senderParam');
        return $stateParams[senderParam];
      },

      /**
       * @ngdoc function
       * @name coyo.domain.SenderModel#searchWithFilter
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Sender elastic search with filter.
       *
       * @param {string} term search term
       * @param {object=} pageable The paging information. If not set an offset of 0 and a page size of 20 will be used.
       * @param {object} filters search filters
       * @param {string[]?} searchFields list of fields to search in (default 'displayName')
       * @param {object} aggregations aggregations
       * @param {boolean} onlyAdmin Boolean flag whether only senders where the user is admin of should be retrieved
       *
       * @returns {promise} An $http promise
       */
      searchWithFilter: function (term, pageable, filters, searchFields, aggregations, onlyAdmin) {
        var url = SenderModel.$url({}, !onlyAdmin ? 'search' : 'search/managed');
        var params = angular.extend({
          term: (term || '').toLowerCase(),
          filters: $httpParamSerializer(filters),
          searchFields: searchFields ? searchFields.join(',') : undefined,
          aggregations: $httpParamSerializer(aggregations)
        }, pageable.getParams());
        return SenderModel.$get(url, params).then(function (response) {
          return new Page(response, params, {
            url: url,
            resultMapper: function (item) {
              return new SenderModel(item);
            }
          });
        });
      }
    });

    // instance members
    angular.extend(SenderModel.prototype, {

      /**
       * @ngdoc method
       * @name coyo.domain.SenderModel#getApps
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Returns all apps of this sender.
       *
       * @returns {array} An array of all apps of this sender or an empty array if none could be found.
       */
      getApps: function () {
        return AppModel.queryWithPermissions({}, {senderId: this.id}, ['manage']);
      },

      /**
       * @ngdoc method
       * @name coyo.domain.SenderModel#getApp
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Returns the app with the given id of this sender.
       *
       * @param {string} appIdOrSlug The ID or slug of the app to return.
       * @returns {object} The app model with the given id.
       */
      getApp: function (appIdOrSlug) {
        var params = SenderModel.applyPermissions(['*']);
        return new AppModel({
          senderId: this.id,
          id: appIdOrSlug
        }).get(null, params);
      },

      /**
       * @ngdoc method
       * @name coyo.domain.SenderModel#addApp
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Adds an app to this sender.
       *
       * @param {string} key The key of the app to add.
       * @param {string} name The name of the app to add.
       * @returns {object} The newly created app model.
       */
      addApp: function (key, name) {
        return new AppModel({
          senderId: this.id,
          key: key,
          name: name
        }).create();
      },

      /**
       * @ngdoc method
       * @name coyo.domain.SenderModel#removeApp
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Removes an app from this sender.
       *
       * @param {string} appIdOrSlug The ID or slug of the app to remove.
       * @returns {object} The deleted app model.
       */
      removeApp: function (appIdOrSlug) {
        return new AppModel({
          senderId: this.id,
          id: appIdOrSlug
        }).delete();
      },

      /**
       * @ngdoc function
       * @name coyo.domain.SenderModel#updateNavigation
       * @methodOf coyo.domain.SenderModel
       *
       * @description
       * Updates the app navigation of this sender.
       *
       * @param {array} appNavigation The new app navigation.
       * @returns {array} The app navigation of this sender.
       */
      updateNavigation: function (appNavigation) {
        var url = AppModel.$url({senderId: this.id}, 'action/navigation');
        return AppModel.$put(url, appNavigation, {});
      }
    });

    return SenderModel;
  }

})(angular);
