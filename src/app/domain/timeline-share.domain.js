(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('TimelineShareModel', TimelineShareModel);

  /**
   * @ngdoc service
   * @name coyo.domain.TimelineShareModel
   *
   * @description
   * Provides the timeline share model.
   *
   * @requires restResourceFactory
   * @requires commons.config.coyoEndpoints
   */
  function TimelineShareModel(restResourceFactory, coyoEndpoints) {
    var TimelineShareModel = restResourceFactory({
      url: coyoEndpoints.timeline.shares
    });

    return TimelineShareModel;
  }

})(angular);
