(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('VideoInfoModel', VideoInfoModel);

  /**
   * @ngdoc service
   * @name VideoInfoModel
   *
   * @description
   * Provides a model for Video Information resolved by the backend.
   *
   * @requires commons.config.coyoEndpoints
   * @requires $http
   */
  function VideoInfoModel(coyoEndpoints, $http) {
    var VideoInfo = {


      getVideoInfo: function (url) {
        return $http({
          method: 'POST',
          url: coyoEndpoints.videoInfo,
          data: {url: url}
        }).then(function (response) {
          response.data.fromBackend = true;
          return response.data;
        });
      }
    };

    return VideoInfo;
  }

})(angular);
