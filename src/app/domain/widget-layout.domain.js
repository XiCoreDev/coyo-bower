(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('WidgetLayoutModel', WidgetLayoutModel);

  /**
   * @ngdoc service
   * @name coyo.domain.WidgetLayoutModel
   *
   * @description
   * Domain model representation of widget layout endpoint.
   *
   * @requires restResourceFactory
   * @requires commons.config.coyoEndpoints
   */
  function WidgetLayoutModel(restResourceFactory, coyoEndpoints) {
    var WidgetLayoutModel = restResourceFactory({
      url: coyoEndpoints.widgetLayouts,
      extensions: ['snapshots']
    });

    return WidgetLayoutModel;
  }

})(angular);
