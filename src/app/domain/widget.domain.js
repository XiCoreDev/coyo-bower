(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('WidgetModel', WidgetModel);

  /**
   * @ngdoc service
   * @name coyo.domain.WidgetModel
   *
   * @description
   * Domain model representation of widget endpoint.
   *
   * @requires restResourceFactory
   * @requires commons.config.coyoEndpoints
   */
  function WidgetModel(restResourceFactory, coyoEndpoints) {
    var Widget = restResourceFactory({
      url: coyoEndpoints.widgets,
      extensions: ['snapshots']
    });

    // class members
    angular.extend(Widget, {
      fromConfig: function (config) {
        return new Widget({
          key: config.key,
          settings: {}
        });
      },
      order: function (slot, parent, widgetIds) {
        var data = {
          widgetIds: widgetIds
        };

        if (parent) {
          angular.extend(data, {
            parentId: parent.id,
            parentType: parent.typeName
          });
        }

        var url = this.$url({slot: slot}, 'action/order');
        var params = Widget.applyPermissions(['manage']);
        return Widget.$put(url, data, {}, params);
      }
    });

    return Widget;
  }

})(angular);
