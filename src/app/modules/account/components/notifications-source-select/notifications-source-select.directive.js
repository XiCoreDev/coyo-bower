(function () {
  'use strict';

  angular
      .module('coyo.account')
      .component('oyocNotificationsSourceSelect', notificationsSourceSelect())
      .controller('NotificationsSourceSelectController', NotificationsSourceSelectController);

  /**
   * @ngdoc directive
   * @name coyo.account.oyocNotificationsSourceSelect:oyocNotificationsSourceSelect
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Renders the options for the browser notification settings of users.
   *
   * @param {object} setting The settings object to display the form for
   * @param {function} onSelectionChange callback function to be executed on selection change
   *
   */
  function notificationsSourceSelect() {
    return {
      templateUrl: 'app/modules/account/components/notifications-source-select/notifications-source-select.html',
      bindings: {
        setting: '=',
        onSelectionChange: '&'
      },
      controller: 'NotificationsSourceSelectController'
    };
  }

  function NotificationsSourceSelectController() {
    var vm = this;

    vm.onChange = vm.onSelectionChange || angular.noop;
    vm.$onInit = _init;

    function _init() {
      _checkProperty(vm.setting, 'properties.notifications.discussion', true);
      _checkProperty(vm.setting, 'properties.notifications.activity', true);
      _checkProperty(vm.setting, 'properties.notifications.post', true);
      _checkProperty(vm.setting, 'properties.notifications.message', true);
    }

    function _checkProperty(obj, prop, val) {
      if (angular.isUndefined(_.get(obj, prop))) {
        _.set(obj, prop, val);
      }
    }
  }
})();
