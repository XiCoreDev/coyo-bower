(function (angular) {
  'use strict';

  angular
      .module('coyo.account')
      .controller('AccountNotificationsController', AccountNotificationsController);

  /**
   * Controller for the account notification settings view
   */
  function AccountNotificationsController($scope, notificationSettings, notificationsSettingsModal,
                                          browserNotificationsService) {
    var vm = this;
    var browserSupportsNotifications = browserNotificationsService.available();

    vm.settings = notificationSettings;

    vm.channelMetadata = {
      BROWSER: {
        icon: 'globe-alt',
        intervalConfigurable: false,
        browserNotificationsConfigurable: browserSupportsNotifications
      },
      EMAIL: {
        icon: 'email',
        intervalConfigurable: true,
        browserNotificationsConfigurable: false
      },
      PUSH: {
        icon: 'smartphone-ring',
        intervalConfigurable: false,
        browserNotificationsConfigurable: false
      }
    };

    vm.browserNotificationsActive = browserNotificationsService.permissionGranted();

    vm.open = open;
    vm.toggle = toggle;
    vm.isEnabled = isEnabled;
    vm.save = save;

    function open(setting) {
      notificationsSettingsModal
          .open(setting, vm.channelMetadata[setting.channel], vm.browserNotificationsActive)
          .then(save);
    }

    function toggle(setting) {
      setting.active = !setting.active;
      save(setting);
    }

    function save(setting) {
      return setting.update().then(function (result) {
        angular.merge(setting, result);
        // Update the reference (needed for the modal)
        for (var i = 0; i < vm.settings.length; ++i) {
          if (vm.settings[i].channel === setting.channel) {
            vm.settings[i] = result;
          }
        }

        if (setting.active && setting.channel === 'BROWSER') {
          browserNotificationsService.requestPermission().then(function (result) {
            _updateBrowserNotificationsActiveFlag(result.result, result.requested);
          });
        }

        return result;
      });
    }

    function isEnabled(channelName) {
      return _.get(vm.channelMetadata, channelName + '.enabled');
    }

    function _updateBrowserNotificationsActiveFlag(result, digest) {
      switch (result) {
      case 'granted':
        vm.browserNotificationsActive = true;
        break;
      default:
      case 'denied':
        vm.browserNotificationsActive = false;
        break;
      }
      if (digest) {
        $scope.$digest();
      }
    }

    (function _init() {
      browserNotificationsService.active(true, false, false, false, false).then(function (isActive) {
        if (isActive) {
          browserNotificationsService.requestPermission().then(function (result) {
            _updateBrowserNotificationsActiveFlag(result.result, result.requested);
          });
        }
      });
    })();
  }

})(angular);
