(function (angular) {
  'use strict';

  angular.module('coyo.admin.apps-widgets')
      .controller('AdminAppListController', AdminAppListController);

  function AdminAppListController($timeout, appRegistry, AppConfigurationModel, appConfigs) {
    var vm = this;

    vm.senderTypes = appRegistry.getAppSenderTypes();
    vm.apps = [];
    vm.toggle = toggle;

    function toggle(senderType, app) {
      $timeout(function () { // wait for msmCheckbox to update model
        var model = new AppConfigurationModel({
          key: app.key,
          senderType: senderType,
          enabled: app.enabledSenderTypes[senderType]
        });
        model.save().then(function (response) {
          app.enabledSenderTypes[senderType] = response.enabledSenderTypes[senderType];
        });
      });
    }

    function _loadApps() {
      vm.apps = _.map(appRegistry.getAll(), function (app) {
        return angular.extend(app, _.find(appConfigs, {key: app.key}));
      });
    }

    (function initController() {
      _loadApps();
    })();
  }

})(angular);
