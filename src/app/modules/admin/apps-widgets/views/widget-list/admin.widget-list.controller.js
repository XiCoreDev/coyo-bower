(function (angular) {
  'use strict';

  angular.module('coyo.admin.apps-widgets')
      .controller('AdminWidgetListController', AdminWidgetListController);

  function AdminWidgetListController($timeout, widgetRegistry, WidgetConfigurationModel, widgetConfigs) {
    var vm = this;

    vm.widgets = [];
    vm.toggle = toggle;

    function toggle(widget) {
      $timeout(function () { // wait for msmCheckbox to update model
        if (widget.enabled) {
          WidgetConfigurationModel.enable(widget.key);
        } else {
          WidgetConfigurationModel.disable(widget.key);
        }
      });
    }

    function _loadWidgets() {
      vm.widgets = _.map(widgetRegistry.getAll(), function (widget) {
        return angular.extend(widget, {
          enabled: _.some(widgetConfigs, {key: widget.key})
        });
      });
    }

    (function initController() {
      _loadWidgets();
    })();
  }

})(angular);
