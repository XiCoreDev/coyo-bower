(function (angular) {
  'use strict';

  angular.module('coyo.admin.settings')
      .controller('AdminGeneralSettingsController', AdminGeneralSettingsController);

  function AdminGeneralSettingsController(msmNotification, SettingsModel, settings, $translate) {
    var vm = this;

    vm.settings = settings;
    vm.save = save;
    vm.$onInit = onInit;

    function save() {
      return settings.update().then(function () {
        SettingsModel.retrieve(true); // reset settings cache
        msmNotification.success('ADMIN.SETTINGS.SAVE.SUCCESS');
      });
    }

    function onInit() {
      $translate('ADMIN.SETTINGS.ANONYMIZE_DELETEDUSERS.DELETED_NAME.DEFAULT').then(function (translation) {
          vm.settings.deletedUserDisplayName = vm.settings.deletedUserDisplayName || translation;
      });
    }
  }

})(angular);
