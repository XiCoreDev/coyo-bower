(function (angular) {
  'use strict';

  angular.module('coyo.admin.settings')
      .controller('AdminNotificationSettingsController', AdminNotificationSettingsController);

  function AdminNotificationSettingsController(msmNotification, SettingsModel, settings) {
    var vm = this;

    vm.settings = settings;
    vm.save = save;

    vm.channelMetadata = {
      BROWSER: {
        key: 'browser',
        id: 'BROWSER'
      },
      EMAIL: {
        key: 'email',
        id: 'EMAIL'
      }
      // PUSH: {
      //   key: 'push',
      //   id: 'PUSH'
      // }
    };

    function save() {
      return settings.update().then(function () {
        SettingsModel.retrieve(true); // reset settings cache
        msmNotification.success('ADMIN.SETTINGS.SAVE.SUCCESS');
      });
    }
  }

})(angular);
