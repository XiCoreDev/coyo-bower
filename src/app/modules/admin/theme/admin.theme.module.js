(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.admin.theme
   *
   * @description
   * # Admin theme management module #
   * The admin theme management module provides views to manage custom css styles.
   */
  angular
      .module('coyo.admin.theme', [
        'coyo.base',
        'coyo.domain'
      ])
      .config(ModuleConfig)
      .constant('adminThemeConfig', {
        templates: {
          theme: 'app/modules/admin/theme/views/theme/admin.theme.html'
        }
      });

  /**
   * Module configuration
   */
  function ModuleConfig($stateProvider, adminThemeConfig) {
    $stateProvider.state('admin.theme', {
      url: '/theme',
      templateUrl: adminThemeConfig.templates.theme,
      controller: 'AdminThemeController',
      controllerAs: '$ctrl',
      data: {
        globalPermissions: 'MANAGE_THEME',
        pageTitle: 'ADMIN.MENU.THEME'
      },
      resolve: {
        backendUrl: function (backendUrlService) {
          return backendUrlService.getUrl();
        },
        themeVariables: function (themeService) {
          return themeService.getThemeVariables();
        },
        customScss: function (themeService) {
          return themeService.getCustomScss();
        }
      }
    });
  }

})(angular);
