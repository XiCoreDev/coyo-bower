(function () {
  'use strict';

  angular.module('coyo.admin.theme')
    .factory('themeService', themeService);

  /**
   * @ngdoc service
   * @name coyo.admin.theme.themeService
   *
   * @description
   * Service to manage the custom css theme
   *
   * @requires $http
   * @requires $q
   * @requires commons.resource.backendUrlService
   * @requires coyo.domain.SettingsModel
   */
  function themeService($http, $q, backendUrlService, SettingsModel, coyoEndpoints) {

    return {
      applyTheme: applyTheme,
      removeTheme: removeTheme,
      updateTheme: updateTheme,
      getThemeVariables: getThemeVariables,
      getCustomScss: getCustomScss
    };

    /**
     * @ngdoc method
     * @name coyo.admin.theme.themeService#applyTheme
     * @methodOf coyo.admin.theme.themeService
     *
     * @description
     * Loads the custom theme css from the backend and applies it to the page.
     *
     * @returns {object} empty promise to be resolved after the css has been applied to the page
     */
    function applyTheme() {
      var backendUrl = backendUrlService.getUrl();
      if (angular.isUndefined(backendUrl)) {
        removeTheme();
        return $q.resolve();
      }
      var url = backendUrl + coyoEndpoints.theme.download;
      return _replaceFavicon().then(function () {
        return $http({
          url: url,
          method: 'GET',
          transformResponse: angular.identity // don't attempt to parse json
        }).then(function (response) {
          removeTheme();
          angular.element('head').append('<style id="customTheme" type="text/css">' + response.data + '</style>');
        });
      });
    }

    /**
     * @ngdoc method
     * @name coyo.admin.theme.themeService#removeTheme
     * @methodOf coyo.admin.theme.themeService
     *
     * @description
     * Removes the applied theme from the page (if exists).
     */
    function removeTheme() {
      angular.element('#customTheme').remove();
    }

    /**
     * @ngdoc method
     * @name coyo.admin.theme.themeService#updateTheme
     * @methodOf coyo.admin.theme.themeService
     *
     * @description
     * Persist the theme settings to the backend.
     *
     * @param {object} All scss variables to be saved.
     * @param {string} customScss The custom SCSS / CSS code to be added to the theme
     * @param {string[]} fileIds The IDs of the files that have been temporarily uploaded and need to be persisted on save.
     *
     * @returns {object} promise returned by $http
     */
    function updateTheme(variables, customScss, fileIds) {
      return $http({
        method: 'PUT',
        url: coyoEndpoints.theme.upload,
        data: {
          variables: variables,
          customScss: customScss,
          fileIds: fileIds
        }
      }).then(function () {
        return SettingsModel.retrieve(true);
      });
    }

    /**
     * @ngdoc method
     * @name coyo.admin.theme.themeService#getThemeVariables
     * @methodOf coyo.admin.theme.themeService
     *
     * @description
     * Returns all SCSS variables from the global settings.
     * The key prefix used in the settings ('theme.*') is removed.
     *
     * @returns {object} promise that resolves to the theme variable object (key -> value)
     */
    function getThemeVariables() {
      var prefix = 'theme.';
      return SettingsModel.retrieve().then(function (settings) {
        return _.mapKeys(_.pickBy(settings, function (value, key) {
          return _.startsWith(key, prefix);
        }), function (value, key) {
          return key.substring(prefix.length);
        });
      });
    }

    /**
     * @ngdoc method
     * @name coyo.admin.theme.themeService#getCustomScss
     * @methodOf coyo.admin.theme.themeService
     *
     * @description
     * Returns the custom SCSS defined in the global settings.
     *
     * @returns {object} promise that resolves to the string with the custom SCSS.
     */
    function getCustomScss() {
      return SettingsModel.retrieveByKey('customScss');
    }

    function _replaceFavicon() {
      return SettingsModel.retrieveByKey('theme.image-coyo-favicon').then(function (icon) {
        if (icon !== null) {
          angular.element('head').find('link[rel="shortcut icon"]').remove();
          angular.element('head').append('<link rel="shortcut icon" href="' + icon.split('\'').join('') + '">');
        }
      });
    }
  }

})();
