(function (angular) {
  'use strict';

  angular.module('coyo.admin.theme')
      .controller('AdminThemeController', AdminThemeController);

  function AdminThemeController(themeService, $q, tempUploadService, Upload, errorService, coyoEndpoints,
                                backendUrl, themeVariables, customScss) {
    var vm = this;
    var tempUploadExpirySeconds = 1800;

    vm.save = save;
    vm.removeAdditionalVariable = removeAdditionalVariable;
    vm.removeImage = removeImage;
    vm.uploadImage = uploadImage;

    vm.predefinedVariableConfig = [
      {key: 'color-primary', displayName: 'ADMIN.THEME.COLORS.COLOR_PRIMARY', defaultValue: '#374555',
        // variables that are set to the same value on save (might be removed when COYOFOUR-1316 is done?)
        aliases: ['color-primary-admin', 'input-border-focus', 'msm-wizard-color', 'msm-modal-header-bg']},
      {key: 'color-secondary', displayName: 'ADMIN.THEME.COLORS.COLOR_SECONDARY', defaultValue: '#242f39'},
      {key: 'color-navbar-border', displayName: 'ADMIN.THEME.COLORS.COLOR_NAVBAR_BORDER', defaultValue: '#9bbf29'},
      {key: 'msm-navbar-text', displayName: 'ADMIN.THEME.COLORS.MSM_NAVBAR_TEXT', defaultValue: '#fdfdfd'},
      {key: 'msm-navbar', displayName: 'ADMIN.THEME.COLORS.MSM_NAVBAR', defaultValue: '#374555'},
      {key: 'btn-primary-color', displayName: 'ADMIN.THEME.COLORS.BTN_PRIMARY_COLOR', defaultValue: '#fff'},
      {key: 'btn-primary-bg', displayName: 'ADMIN.THEME.COLORS.BTN_PRIMARY_BG', defaultValue: '#374555'},
      {key: 'color-background-main', displayName: 'ADMIN.THEME.COLORS.COLOR_BACKGROUND_MAIN', defaultValue: '#e8e8e8'}
    ];

    vm.imageConfig = [{
      key: 'image-coyo-front',
      retinaKey: 'image-coyo-front-hd',
      displayName: 'ADMIN.THEME.LOGOS.IMAGE_COYO_FRONT.DISPLAY_NAME',
      help: 'ADMIN.THEME.LOGOS.IMAGE_COYO_FRONT.HELP',
      defaultUrl: '/assets/images/logos/coyo/logo-coyo-inversed-front-hd.png',
      width: 476,
      // workaround for bug in chrome, where image/* causes the file select dialog to open very slowly
      // http://stackoverflow.com/questions/39187857/inputfile-accept-image-open-dialog-so-slow-with-chrome
      acceptTypes: 'image/png, image/jpeg, image/gif',
      // variables are set to the result of the given fn (can be a promise), after a new image has been selected
      additionalVariables: {
        'height-image-front': function (originalFile, resizedFile) {
          return Upload.imageDimensions(resizedFile).then(function (dimensions) {
            return dimensions.height + 'px';
          });
        }
      }
    }, {
      key: 'image-coyo-nav',
      retinaKey: 'image-coyo-nav-hd',
      displayName: 'ADMIN.THEME.LOGOS.IMAGE_COYO_NAV.DISPLAY_NAME',
      help: 'ADMIN.THEME.LOGOS.IMAGE_COYO_NAV.HELP',
      defaultUrl: '/assets/images/logos/coyo/logo-coyo-inversed-nav-hd.png',
      height: 50,
      acceptTypes: 'image/png, image/jpeg, image/gif',
      // variables are set to the result of the given fn (can be a promise), after a new image has been selected
      additionalVariables: {
        'width-navbar-brand': function (originalFile, resizedFile) {
          return Upload.imageDimensions(resizedFile).then(function (dimensions) {
            return dimensions.width + 'px';
          });
        }
      }
    }, {
      key: 'image-coyo-favicon',
      displayName: 'ADMIN.THEME.LOGOS.IMAGE_COYO_FAVICON.DISPLAY_NAME',
      help: 'ADMIN.THEME.LOGOS.IMAGE_COYO_FAVICON.HELP',
      defaultUrl: '/assets/images/logos/coyo/favicon.ico',
      width: 32,
      height: 32,
      acceptTypes: 'image/x-icon'
    }];

    function save() {
      delete vm.errorMessage;
      return themeService.updateTheme(_collectScssVariables(), vm.customScss, _collectUploadFileIds())
          .then(function () {
            _clearUploadedFileIds();
            return themeService.applyTheme();
          })
          .catch(function (errorResponse) {
            if (_.get(errorResponse, 'data.errorStatus') === 'THEME_CREATE_ERROR') {
              vm.errorMessage = _.get(errorResponse, 'data.context.themeError');
              errorService.suppressNotification(errorResponse);
            }
          });
    }

    function uploadImage(imageKey) {
      var imageConfig = _.find(vm.imageConfig, {key: imageKey});
      var fileToUpload = imageConfig.data.fileToUpload;
      if (!fileToUpload) {
        return;
      }

      // regular size
      _resize(fileToUpload, imageConfig, false).then(function (file) {
        tempUploadService.upload(file, tempUploadExpirySeconds).then(function (blob) {
          imageConfig.data.file = file;
          imageConfig.data.uid = blob.uid;
          _setImageVariable(imageConfig.key, _createUrl(blob.uid));
          _.forEach(Object.keys(_.get(imageConfig, 'additionalVariables', {})), function (key) {
            var fn = imageConfig.additionalVariables[key];
            $q.when(fn(fileToUpload, file)).then(function (value) {
              _setImageVariable(key, value);
            });
          });
        });
      });

      // retina size
      if (imageConfig.retinaKey) {
        _resize(fileToUpload, imageConfig, true).then(function (file) {
          tempUploadService.upload(file, tempUploadExpirySeconds).then(function (blob) {
            imageConfig.data.retinaFile = file;
            imageConfig.data.retinaUid = blob.uid;
            _setImageVariable(imageConfig.retinaKey, _createUrl(blob.uid));
          });
        });
      }
    }

    function removeImage(imageKey) {
      var imageConfig = _.find(vm.imageConfig, {key: imageKey});
      imageConfig.data = {};
      _.concat(imageConfig.key, imageConfig.retinaKey, _.keys(imageConfig.additionalVariables))
          .forEach(_removeImageVariable);
    }

    function removeAdditionalVariable(index) {
      vm.additionalVariables.splice(index, 1);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    function _collectScssVariables() {
      var scssVariables = {};
      _.forEach(vm.predefinedVariableConfig, function (variable) {
        if (variable.value) {
          scssVariables[variable.key] = variable.value;
          _.forEach(variable.aliases, function (alias) {
            scssVariables[alias] = variable.value;
          });
        }
      });
      _.concat(vm.additionalVariables, vm.imageVariables).forEach(function (variable) {
        scssVariables[variable.key] = variable.value;
      });
      return scssVariables;
    }

    function _collectUploadFileIds() {
      var fileIds = [];
      vm.imageConfig.forEach(function (image) {
        if (image.data.uid) {
          fileIds.push(image.data.uid);
        }
        if (image.data.retinaUid) {
          fileIds.push(image.data.retinaUid);
        }
      });
      return fileIds;
    }

    function _clearUploadedFileIds() {
      vm.imageConfig.forEach(function (image) {
        delete image.data.uid;
        delete image.data.retinaUid;
      });
    }

    function _isPredefinedVariable(key) {
      return _.concat(
          _.map(vm.predefinedVariableConfig, 'key'),
          _.flatMap(_.map(vm.predefinedVariableConfig, 'aliases'))
      ).indexOf(key) >= 0;
    }

    function _isImageVariable(key) {
      return _getImageVariableKeys().indexOf(key) >= 0;
    }

    function _getImageVariableKeys() {
      return _.concat(
          _.map(vm.imageConfig, 'key'),
          _.map(vm.imageConfig, 'retinaKey'),
          _.flatMap(_.map(vm.imageConfig, function (image) {
            return _.keys(image.additionalVariables);
          }))
      );
    }

    function _setImageVariable(key, value) {
      _removeImageVariable(key);
      vm.imageVariables.push({key: key, value: value});
    }

    function _removeImageVariable(key) {
      _.remove(vm.imageVariables, {key: key});
    }

    function _createUrl(uid) {
      return '\'' + backendUrl + coyoEndpoints.theme.files.replace('{id}', uid) + '\'';
    }

    function _resize(file, config, isRetina) {
      var factor = isRetina ? 2 : 1;
      if (!config.width && !config.height) {
        return $q.resolve(file);
      }

      var targetWidth = config.width ? config.width * factor : null;
      var targetHeight = config.height ? config.height * factor : null;

      return Upload.resize(file, targetWidth, targetHeight, null, null, null, null, function resizeIf(width, height) {
        if (config.width && config.width * factor > width) {
          return false;
        }
        if (config.height && config.height * factor > height) {
          return false;
        }
        return true;
      });
    }

    (function _init() {
      vm.activeTab = 'colors';

      _.forEach(vm.predefinedVariableConfig, function (variable) {
        variable.value = themeVariables[variable.key];
      });

      vm.imageConfig.forEach(function (image) {
        _.set(image, 'data.url', _.replace(themeVariables[image.key], /'/g, ''));
      });
      vm.imageVariables = _.map(_getImageVariableKeys(), function (key) {
        return {key: key, value: themeVariables[key]};
      });

      // all remaining vars that are not used for predefined vars or images end up in the advanced tab
      vm.additionalVariables = _.map(_.pickBy(themeVariables, function (value, key) {
        return !_isPredefinedVariable(key) && !_isImageVariable(key);
      }), function (value, key) {
        return {key: key, value: value};
      });

      vm.customScss = customScss;
    })();

  }

})(angular);
