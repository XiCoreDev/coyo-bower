(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.admin.translations
   *
   * @description
   * # Admin translations management module #
   * The admin translations management module provides views to manage custom translation keys.
   */
  angular
      .module('coyo.admin.translations', [
        'coyo.base',
        'coyo.domain'
      ])
      .config(ModuleConfig)
      .constant('adminTranslationsConfig', {
        templates: {
          list: 'app/modules/admin/translations/views/list/admin.translations.html',
          edit: 'app/modules/admin/translations/views/edit/admin.translations-edit.html'
        }
      });

  /**
   * Module configuration
   */
  function ModuleConfig($stateProvider, adminTranslationsConfig) {
    $stateProvider.state('admin.translations', {
      url: '/translations',
      templateUrl: adminTranslationsConfig.templates.list,
      controller: 'AdminTranslationsController',
      controllerAs: '$ctrl',
      data: {
        globalPermissions: 'MANAGE_LANGUAGES',
        pageTitle: 'ADMIN.MENU.TRANSLATIONS'
      },
      resolve: {
        settings: function (SettingsModel) {
          return SettingsModel.retrieve();
        }
      }
    });
  }

})(angular);
