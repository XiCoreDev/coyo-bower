(function (angular) {
  'use strict';

  angular
      .module('coyo.admin.translations')
      .controller('AdminTranslationsController', AdminTranslationsController);

  function AdminTranslationsController($q, $translate, $timeout, $rootScope, $scope, LanguagesModel, translationRegistry, msmModal, settings) {
    var vm = this;
    vm.filter = {
      'status': {
        'language': filterLanguage,
        'translated': filterTranslatedKeys,
        'onlyTranslatedKeys': false
      },
      'languages': [],
      'search': searchFilter,
      'searchTerm': '',
      'activeFilterTranslated': false
    };

    var allTranslationKeys;
    var filteredTranslationKeys;

    vm.editKey = editKey;
    vm.saveKey = saveKey;
    vm.getTranslated = getTranslated;
    vm.currentPage = currentPage;
    vm.deleteAllKeys = deleteAllKeys;
    vm.onTranslateKeypress = onTranslateKeypress;
    vm.toggleLanguageMenu = toggleLanguageMenu;
    vm.pageSize = 20;
    vm.page = 1;
    vm.reloadTable = false;
    vm.filterOpen = false;

    // need to know about screen size in ctrl to turn off infinite scrolling in desktop mode
    var unsubscribe = $rootScope.$on('screenSize:changed', function (event, screenSize) {
      // move back to first page when switching between mobile and desktop view
      var isMobile = screenSize.isXs || screenSize.isSm;
      if (isMobile !== vm.mobile && vm.page > 1) {
        vm.page = 1;
      }
      vm.mobile = isMobile;
    });
    $scope.$on('$destroy', unsubscribe);

    vm.mobile = $rootScope.screenSize.isXs || $rootScope.screenSize.isSm;

    if (vm.mobile) {
      vm.page = 0;
    }

    // returns the index of the current page
    function currentPage() {
      return (vm.pageSize * (vm.page - 1));
    }

    function filterLanguage(languageKey) {
      vm.filter.activeLanguage = languageKey;
      vm.translationKeys = _getTranslationTable(languageKey);
      allTranslationKeys = vm.translationKeys;
      filteredTranslationKeys = vm.translationKeys;
      vm.filter.status.onlyTranslatedKeys = false;
      vm.filter.searchTerm = '';
      vm.filter.activeFilterTranslated = false;
      vm.page = 1;
      toggleLanguageMenu(false);
      _loadMessageKeys(vm.filter.activeLanguage);
    }

    function filterTranslatedKeys() {
      vm.filter.activeFilterTranslated = !vm.filter.activeFilterTranslated;
      var tableArray = [];
      vm.filter.searchTerm = '';

      if (vm.filter.activeFilterTranslated) {
        _.forEach(allTranslationKeys, function (value) {
          if (getTranslated(value.key)) {
            tableArray.push({key: value.key, value: value.value});
          }
        });
        vm.translationKeys = tableArray;
        filteredTranslationKeys = vm.translationKeys;
      } else {
        vm.translationKeys = allTranslationKeys;
        filteredTranslationKeys = vm.translationKeys;
      }
    }

    function toggleLanguageMenu(value) {
      $timeout(function () { // delay opening to avoid firing click-outside event
        vm.filterOpen = angular.isDefined(value) ? value : !vm.filterOpen;
        var fn = vm.filterOpen ? 'addClass' : 'removeClass';
        angular.element('#translation-language-menu')[fn]('active');
      });
    }

    function onTranslateKeypress($event) {
      if ($event.keyCode === 13) {
        angular.element($event.currentTarget).blur();
      }
    }

    function searchFilter(search) {
      var results = [];
      vm.filter.searchTerm = search;
      vm.translationKeys = filteredTranslationKeys;
      var i = 0;

      if (search) {
        _.forEach(vm.translationKeys, function (value) {
          var messageTranslation = getTranslated(value.key) ? getTranslated(value.key).translation : '';

          if (_.includes(_.lowerCase(value.key), _.lowerCase(search)) ||
              _.includes(_.lowerCase(value.value), _.lowerCase(search)) ||
              _.includes(_.lowerCase(messageTranslation), _.lowerCase(search))) {
            results[i] = value;
            i++;
          }
        });
        vm.translationKeys = results;
      }
    }

    function editKey($event) {
      angular.element($event.currentTarget)
             .addClass('edit')
             .find('.input')
             .focus()
             .select();

      angular.element($event.currentTarget.parentNode).removeClass('override');
    }

    function saveKey($event, key) {
      var messageKey = getTranslated(key) ? getTranslated(key) : null;
      // remove trailing whitespace with trim
      var value = $event.currentTarget.value.trim();
      // update value in table (without trailing whitespace)
      $event.currentTarget.value = value;
      var promise;

      if (value) {
        angular.element($event.currentTarget.parentNode.parentNode).addClass('override');
        if (messageKey) {
          promise = LanguagesModel.updateTranslation(vm.filter.activeLanguage, key, value);
        } else {
          promise = LanguagesModel.createTranslation(vm.filter.activeLanguage, key, value);
        }
      } else {
        angular.element($event.currentTarget.parentNode).removeClass('edit');
        if (messageKey) {
          promise = LanguagesModel.deleteTranslation(vm.filter.activeLanguage, key);
        } else {
          promise = $q.resolve();
        }
      }

      promise.then(function () {
        _loadMessageKeys(vm.filter.activeLanguage).then(function () {
          searchFilter(vm.filter.searchTerm);
        });
      });
    }

    function deleteAllKeys() {
      msmModal.confirm({
        title: 'ADMIN.TRANSLATIONS.MODAL.RESET.HEADLINE',
        text: 'ADMIN.TRANSLATIONS.MODAL.RESET.TEXT',
        close: {title: 'RESET_ALL', style: 'btn-danger'},
        dismiss: {title: 'NO'}
      }).result.then(function () {
        vm.reloadTable = true;
        LanguagesModel.deleteTranslations(vm.filter.activeLanguage).then(function () {
          _loadMessageKeys(vm.filter.activeLanguage).then(function () {
            if (vm.filter.activeFilterTranslated) {
              filterTranslatedKeys();
            }
            searchFilter(vm.filter.searchTerm);
            vm.filter.status.onlyTranslatedKeys = false;
            vm.reloadTable = false;
          });
        });
      });
    }

    // Return true if the translation key in the current line was changed
    function getTranslated(key) {
      return _.find(vm.translatedKeys, function (messageKey) {
        return messageKey.key === key;
      });
    }

    function _loadMessageKeys(language) {
      return LanguagesModel.getTranslations(language).then(function (keys) {
        vm.translatedKeys = keys;
      });
    }

    function _getTranslationTable(language) {
      var tableObject = translationRegistry.getTranslationTable(language);
      var tableArray = [];

      _.forIn(tableObject, function (value, key) {
        tableArray.push({key: key, value: value});
      });

      return tableArray;
    }

    (function _init() {
      // Available language keys
      vm.filter.languages = $translate.getAvailableLanguageKeys();
      // current language
      vm.filter.activeLanguage = settings.defaultLanguage.toLowerCase();
      // translation table of current language
      vm.translationKeys = _getTranslationTable(vm.filter.activeLanguage);
      allTranslationKeys = vm.translationKeys;
      filteredTranslationKeys = vm.translationKeys;

      _loadMessageKeys(vm.filter.activeLanguage);
    })();
  }
})(angular);
