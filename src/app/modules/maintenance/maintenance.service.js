(function (angular) {
  'use strict';

  angular
      .module('coyo.maintenance')
      .factory('maintenanceService', maintenanceService);

  /**
   * @ngdoc service
   * @name coyo.modules.maintenance.MaintenanceService
   *
   * @description
   * Maintenance service
   *
   * @requires $http
   * @requires commons.config.coyoEndpoints
   */
  function maintenanceService($http, coyoEndpoints) {
    return {
      getCurrentMaintenanceInfo: getCurrentMaintenanceInfo
    };

    /**
     * @ngdoc function
     * @name coyo.modules.maintenance.MaintenanceService#getCurrentMaintenanceInfo
     * @methodOf coyo.modules.maintenance.MaintenanceService
     *
     * @description
     * Gets the currently active maintenance information
     *
     * @returns {promise} A promise
     */
    function getCurrentMaintenanceInfo() {
      return $http.get(coyoEndpoints.maintenance.info).then(function (result) {
        return result.data;
      });
    }
  }
})(angular);
