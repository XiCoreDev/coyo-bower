(function (angular) {
  'use strict';

  angular
      .module('coyo.pages')
      .controller('PagesCreateController', PagesCreateController);

  function PagesCreateController($q, $state, currentUser, PageModel, msmNotification, $timeout) {
    var vm = this;

    vm.back = back;
    vm.next = next;
    vm.$onInit = _init;

    function back() {
      vm.wizard.active = Math.max(0, vm.wizard.active - 1);
    }

    function next(form) {
      if (form && form.$valid) {
        if (vm.wizard.active < vm.wizard.states.length - 1) {
          return $q.resolve(vm.wizard.active++);
        } else {
          vm.page.categoryIds = _.map(vm.page.categories, 'id');

          // show help text if auto-subscribe + saving takes more than a few seconds
          var cancel;
          if (vm.page.autoSubscribeType !== 'NONE') {
            cancel = $timeout(function () {
              vm.showAutoSubscribeInfo = true;
            }, 5000);
          }

          return vm.page.create().then(function (page) {
            $state.go('main.page.show', {idOrSlug: page.slug});
            msmNotification.success('MODULE.PAGES.CREATE.SUCCESS');
          }).finally(function () {
            if (cancel) {
              $timeout.cancel(cancel);
            }
            vm.showAutoSubscribeInfo = false;
          });
        }
      }
      return $q.reject();
    }

    function _init() {
      vm.wizard = {
        states: ['MODULE.PAGES.CREATE.GENERAL', 'MODULE.PAGES.CREATE.ACCESS'],
        active: 0
      };
      if (currentUser.hasGlobalPermissions('AUTO_SUBSCRIBE_PAGE')) {
        vm.wizard.states.push('MODULE.PAGES.AUTO_SUBSCRIBE.LABEL');
      }

      vm.page = new PageModel({
        visibility: currentUser.hasGlobalPermissions('CREATE_PUBLIC_PAGE') ? 'PUBLIC' : 'PRIVATE',
        adminIds: [currentUser.id],
        adminGroupIds: [],
        memberIds: [],
        memberGroupIds: [],
        autoSubscribeType: 'NONE'
      });
    }
  }

})(angular);
