(function (angular) {
  'use strict';

  angular
      .module('coyo.pages')
      .controller('PagesListController', PagesListController);

  function PagesListController($rootScope, $scope, $q, $sessionStorage, $state, $stateParams, $timeout, PageModel,
                               PageCategoryModel, Pageable, currentUser, categories, appService, authService,
                               msmModal, subscriptionsService, pagesConfig) {
    var vm = this;

    vm.currentUser = currentUser;
    vm.treeOptions = _buildTreeOptions();
    vm.categories = categories;
    vm.subscriptions = [];

    vm.openPage = openPage;
    vm.openApp = openApp;
    vm.isAutoSubscribe = isAutoSubscribe;
    vm.search = search;
    vm.getTotalCount = getTotalCount;
    vm.isCategoryActive = isCategoryActive;
    vm.toggleCategory = toggleCategory;
    vm.addCategory = addCategory;
    vm.editCategory = editCategory;
    vm.saveCategory = saveCategory;
    vm.onCategoryKeypress = onCategoryKeypress;
    vm.onCategoryBlur = onCategoryBlur;
    vm.deleteCategory = deleteCategory;

    function openPage(page) {
      $state.go('main.page.show', {idOrSlug: page.slug}, {inherit: false});
    }

    function openApp(page, app) {
      appService.redirectToApp(page, app);
    }

    function _loadPages() {
      if (vm.loading) {
        return;
      }

      // write params to URL
      $state.transitionTo('main.page', _.omitBy({
        term: _.get(vm.query, 'term', ''),
        'categories[]': _.get(vm.query, 'filters.categories', [])
      }, _.isEmpty), {notify: false});

      // perform search
      $sessionStorage.pageQuery = vm.query;
      vm.loading = true;

      var term = vm.query.term;
      var sort = term ? ['_score,DESC', 'displayName.sort'] : 'displayName.sort';
      var pageable = new Pageable(0, pagesConfig.paging.pageSize, sort);
      var filters = vm.query.filters;
      var fields = ['displayName'];
      var aggregations = {categories: 0};
      PageModel.searchWithFilter(term, pageable, filters, fields, aggregations).then(function (page) {
        vm.currentPage = page;

        _.forEach(vm.categories, function (category) {
          var data = _.find(page.aggregations.categories, {key: category.id});
          category.count = _.get(data, 'count', 0);
        });
        var missingData = _.find(page.aggregations.categories, {key: 'N/A'});
        vm.missingCount = _.get(missingData, 'count', null);

        // fetch subscriptions to check for auto subscribe
        subscriptionsService.getSubscriptions(vm.currentUser.id, _.map(page.content, 'id')).then(function (subscriptions) {
          vm.subscriptions.push.apply(vm.subscriptions, subscriptions);
        });

      }).finally(function () {
        vm.loading = false;
      });
    }

    function isAutoSubscribe(page) {
      return _.some(vm.subscriptions, {targetId: page.id, autoSubscribe: true}) || undefined;
    }

    function search(searchTerm) {
      if (!vm.editingCategory) {
        vm.query.term = searchTerm;
        vm.query.filters = {categories: []};
        _loadPages();
      }
    }

    /* ===== Categories ===== */

    vm.editingCategory = null;
    vm.savingCategory = false;

    function getTotalCount() {
      return _.sumBy(vm.categories, 'count') + (vm.missingCount || 0);
    }

    function isCategoryActive(category) {
      var active = _.get(vm.query, 'filters.categories', []);
      return category ? active.indexOf(category.id) !== -1 : active.length === 0;
    }

    function toggleCategory(category) {
      if (!vm.editingCategory) {
        vm.query.filters = {categories: category ? [category.id] : []};
        _loadPages();
      }
    }

    function addCategory($event) {
      if (vm.editingCategory || vm.savingCategory) {
        return;
      }
      vm.editingCategory = new PageCategoryModel();
      vm.categories.push(vm.editingCategory);

      $timeout(function () {
        var target = angular.element($event.currentTarget);
        target.parent().find('input').select();
      });
    }

    function editCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (vm.editingCategory || vm.savingCategory) {
        return;
      }
      vm.editingCategory = angular.copy(category);

      $timeout(function () {
        var target = angular.element($event.currentTarget);
        target.parent().parent().find('input').select();
      });
    }

    function saveCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (!category.name || vm.savingCategory) {
        return $q.reject();
      }

      vm.savingCategory = true;
      return category.save().then(function (result) {
        vm.categories.splice(_.findIndex(vm.categories, {id: category.id}), 1, result);
      }).finally(function () {
        vm.savingCategory = false;
        vm.editingCategory = null;
      });
    }

    function onCategoryKeypress(category, $event) {
      if ($event.keyCode === 13) {
        saveCategory(category, $event);
      }
    }

    function onCategoryBlur(category, $event) {
      if (!vm.editingCategory || !category) {
        return; // not in edit mode
      } else if ($event) {
        var currentTarget = angular.element($event.currentTarget);
        var relatedTarget = angular.element($event.relatedTarget);
        if (currentTarget[0] === relatedTarget.find('input')[0]) {
          return; // clicked on current filter
        }
      }

      $timeout(function () {
        if (category.isNew()) {
          vm.categories.pop();
        } else {
          category.name = vm.editingCategory.name;
        }
        vm.editingCategory = null;
      });
    }

    function deleteCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (vm.editingCategory || vm.savingCategory) {
        return $q.reject();
      }

      return msmModal.confirm({
        title: 'PAGE.CATEGORY.DELETE.MODAL.TITLE',
        text: 'PAGE.CATEGORY.DELETE.MODAL.TEXT',
        translationContext: {category: category.name},
        close: {title: 'YES'},
        dismiss: {title: 'NO'}
      }).result.then(function () {
        vm.savingCategory = true;
        return category.delete().then(function (result) {
          _.remove(vm.categories, {id: category.id});
          if (category.id === _.get(vm.query, 'filters.categories[0]', null)) {
            vm.query.filters = {categories: []};
            _loadPages();
          }
          return result;
        });
      }).finally(function () {
        vm.savingCategory = false;
      });
    }

    /* ==================== */

    function _buildTreeOptions() {
      return {
        dropped: function (event) {
          // persist new sort order
          if (event.source.index !== event.dest.index) {
            PageCategoryModel.order(_.map(vm.categories, 'id'));
          }
        }
      };
    }

    (function _init() {
      // extract search from URL / storage
      vm.query = $sessionStorage.pageQuery || {};
      if ($stateParams.term || $stateParams['categories[]']) {
        angular.extend(vm.query, {
          term: $stateParams.term,
          filters: {categories: $stateParams['categories[]']}
        });
      }

      // register ESC callback
      var unsubscribeEscFn = $rootScope.$on('keyup:esc', function () {
        if (vm.editingCategory) {
          var category = _.find(vm.categories, function (c) {
            return vm.editingCategory.isNew() ? c.isNew() : vm.editingCategory.id === c.id;
          });
          onCategoryBlur(category);
        }
      });

      // register permission callback
      var unsubscribePermissionFn = authService.onGlobalPermissions('MANAGE_PAGE_CATEGORIES', function (canManage) {
        vm.canManagePageCategories = canManage;
      });

      // unregister callbacks on $scope death
      $scope.$on('$destroy', function () {
        unsubscribeEscFn();
        unsubscribePermissionFn();
      });

      $timeout(function () {
        _loadPages();
      });
    })();
  }

})(angular);
