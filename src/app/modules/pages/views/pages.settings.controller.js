(function (angular) {
  'use strict';

  angular
      .module('coyo.pages')
      .controller('PageSettingsController', PageSettingsController);

  /**
   * Controller for the page edit view
   */
  function PageSettingsController($state, msmModal, msmNotification, page, members, $timeout) {
    var vm = this;

    vm.save = savePage;
    vm.delete = deletePage;
    vm.$onInit = onInit;

    function savePage() {
      vm.page.categoryIds = _.map(vm.page.categories, 'id');

      // show help text if auto-subscribe + saving takes more than a few seconds
      var cancel;
      if (vm.page.autoSubscribeType !== 'NONE' || vm.page.autoSubscribeType !== vm.oldAutoSubscribeType) {
        cancel = $timeout(function () {
          vm.showAutoSubscribeInfo = true;
        }, 5000);
      }

      return vm.page.update().then(function (page) {
        msmNotification.success('MODULE.PAGES.EDIT.SUCCESS');

        if ($state.is('main.page.show.settings')) {
          $state.go('main.page.show', {idOrSlug: page.slug}, {reload: 'main.page.show'});
        }
      }).finally(function () {
        if (cancel) {
          $timeout.cancel(cancel);
        }

        vm.showAutoSubscribeInfo = false;
      });
    }

    function deletePage() {
      msmModal.confirm({title: 'PAGE.DELETE', text: 'PAGE.DELETE.CONFIRM'}).result.then(function () {
        vm.page.delete().then(function () {
          $state.go('main.page');
          msmNotification.success('PAGE.DELETE.SUCCESS');
        });
      });
    }

    function onInit() {
      vm.baseUrl = $state.href('main.page', {}) + '/';
      vm.oldName = page.displayName;
      vm.oldSlug = page.slug;
      vm.oldAutoSubscribeType = page.autoSubscribeType;

      vm.page = angular.extend(page, members);
      _.forEach(vm.page.categories, function (category) {
        category.displayName = category.name;
      });
    }
  }

})(angular);
