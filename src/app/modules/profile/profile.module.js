(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.profile
   *
   * @description
   * # Profile module #
   * The profile module renders the profile information and provides methods for accessing and manipulating the user data.
   *
   * Available components are for example
   * * the profile fields,
   * * the push devices,
   * * the user avatar,
   * * the user avatar overlay for the image upload,
   * * the user follow button and
   * * the user list item.
   *
   * @requires $stateProvider
   */
  angular
    .module('coyo.profile', [
      'coyo.base',
      'commons.auth',
      'commons.config',
      'commons.ui',
      'commons.i18n',
      'commons.target',
      'commons.messaging'
    ])
    .config(ModuleConfig)
    .constant('profileConfig', {
      templates: {
        main: 'app/modules/profile/views/profile.main.html',
        overview: 'app/modules/profile/views/profile.overview.html',
        activity: 'app/modules/profile/views/profile.activity.html',
        info: 'app/modules/profile/views/profile.info.html',
        blog: 'app/modules/profile/views/profile.blog.html'
      }
    });

  /**
   * Module configuration
   */
  function ModuleConfig($stateProvider, profileConfig) {
    $stateProvider
      .state('main.profile', {
        url: '/profile/:userId',
        templateUrl: profileConfig.templates.main,
        controller: 'ProfileMainController',
        controllerAs: 'profileCtrl',
        data: {
          guide: 'user/user-profile',
          pageTitle: false
        },
        redirectTo: 'main.profile.activity',
        resolve: {
          currentUser: function (authService) {
            return authService.getUser();
          },
          user: function ($stateParams, UserModel, currentUser) {
            if (!$stateParams.userId || currentUser.id === $stateParams.userId) {
              return currentUser;
            }
            return UserModel.getWithPermissions({id: $stateParams.userId}, {}, ['manage']);
          },
          profileFieldGroups: function (profileFieldsService) {
            return profileFieldsService.getGroups();
          },
          linkPattern: function (SettingsModel) {
            return SettingsModel.retrieveByKey('linkPattern');
          },
          emailPattern: function (SettingsModel) {
            return SettingsModel.retrieveByKey('emailPattern');
          },
          phonePattern: function (SettingsModel) {
            return SettingsModel.retrieveByKey('phonePattern');
          }
        },
        onEnter: function (user, currentUser, $state, authService, msmNotification, titleService) {
          var permission = user.id === currentUser.id ? 'ACCESS_OWN_USER_PROFILE' : 'ACCESS_OTHER_USER_PROFILE';
          if (!currentUser.hasGlobalPermissions(permission)) {
            msmNotification.error('ERRORS.FORBIDDEN');
            $state.go('main');
          } else {
            titleService.set(user.displayName, false);
          }
        }
      })
      .state('main.profile.current', {
        onEnter: function (authService, $state) {
          authService.getUser().then(function (currentUser) {
            $state.go('main.profile', {userId: currentUser.id});
          });
        }
      })
      .state('main.profile.overview', {
        url: '/overview',
        templateUrl: profileConfig.templates.overview
      })
      .state('main.profile.activity', {
        url: '/activity',
        templateUrl: profileConfig.templates.activity
      })
      .state('main.profile.info', {
        url: '/info',
        templateUrl: profileConfig.templates.info
      })
      .state('main.profile.blog', {
        url: '/blog',
        templateUrl: profileConfig.templates.blog
      });
  }

})(angular);
