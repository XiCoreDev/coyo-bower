(function (angular) {
  'use strict';

  angular
      .module('coyo.timeline')
      .controller('TimelineFormController', TimelineFormController);

  function TimelineFormController($scope, $rootScope, $translate, TimelineItemModel, authService, tempUploadService, msmUtil) {
    var vm = this;
    var POST_AS_TRANSLATION = $translate.instant('MODULE.TIMELINE.FORM.POST_AS');

    vm.screenSize = $rootScope.screenSize;
    vm.staticSenderOptions = [];
    vm.selectSenderOptionGroups = selectSenderOptionGroups;

    if (!vm.contextId) {
      throw new Error('[TimelineFormController] Context ID required but not found.');
    }

    if (!vm.timelineType) {
      throw new Error('[TimelineFormController] Timeline type required but not found.');
    }

    var inModal = angular.isDefined($scope.vm);

    // load current user
    authService.getUser().then(function (user) {
      vm.currentUser = user;
      _reset();
    });

    vm.save = save;
    vm.onKeyDown = onKeyDown;
    vm.addAttachments = addAttachments;
    vm.removeAttachment = removeAttachment;
    vm.uuid = msmUtil.uuid();

    vm.stickyExpiryOptions = [
      {expiry: 1000 * 60 * 60 * 24, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_DAY'},
      {expiry: 1000 * 60 * 60 * 24 * 7, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_WEEK'},
      {expiry: 1000 * 60 * 60 * 24 * 30, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_MONTH'},
      {expiry: 1000 * 60 * 60 * 24 * 365, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_YEAR'}
    ];

    // --------------------------------------------------------------------------------------------------------

    function _reset() {
      vm.newItemAttachments = [];
      vm.author = vm.currentUser;
      vm.currentUser.typeLabelTranslated = $translate.instant('MODULE.TIMELINE.FORM.POST_AS_CURRENT_USER_SUBLINE');
      vm.staticSenderOptions = [vm.currentUser];
      vm.newItemModel = new TimelineItemModel({
        authorId: vm.currentUser.id,
        type: 'post'
      });
      vm.stickyExpiry = null;

      if (vm.form) {
        vm.form.$setPristine();
        vm.form.$setUntouched();
      }
    }

    // --------------------------------------------------------------------------------------------------------

    function save() {
      if (vm.form.$valid && _.filter(vm.newItemAttachments, {uploading: true}).length === 0) {
        vm.saving = true;

        vm.newItemModel.authorId = vm.author ? vm.author.id : vm.currentUser.id;
        vm.newItemModel.recipientId = vm.timelineType === 'personal' ? vm.author.id : vm.contextId;
        vm.newItemModel.stickyExpiry = vm.stickyExpiry ? new Date().getTime() + vm.stickyExpiry : null;

        vm.newItemModel.attachments = _.map(vm.newItemAttachments, function (attachment) {
          return {
            name: attachment.name,
            uid: attachment.uid,
            contentType: attachment.contentType
          };
        });

        return vm.newItemModel.create().then(_reset).finally(function () {
          vm.saving = false;

          if (inModal) {
            $scope.$dismiss('cancel'); //eslint-disable-line
          }
        });
      } else {
        return null;
      }
    }

    function onKeyDown($event) {
      if ($event.ctrlKey && $event.keyCode === 13) {
        $event.preventDefault();
        vm.save();
      }
    }

    function addAttachments(files) {
      vm.newItemAttachments.push.apply(vm.newItemAttachments, files);

      angular.forEach(files, function (file) {
        tempUploadService.upload(file, 300).then(function (blob) {
          angular.extend(file, blob);
        }).catch(function () {
          removeAttachment(file);
        });
      });

      vm.focusMessageFormField = true;
    }

    function removeAttachment(file) {
      vm.newItemAttachments = _.reject(vm.newItemAttachments, file);
      vm.focusMessageFormField = true;
    }

    function selectSenderOptionGroups(option) {
      return vm.staticSenderOptions.indexOf(option) > -1 ? POST_AS_TRANSLATION : undefined;
    }
  }

})(angular);
