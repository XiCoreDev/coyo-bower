(function (angular) {
  'use strict';

  angular
      .module('coyo.timeline')
      .directive('coyoTimelineItem', timelineItem)
      .controller('TimelineItemDirectiveController', TimelineItemDirectiveController);

  /**
   * @ngdoc directive
   * @name coyo.timeline.coyoTimelineItem:coyoTimelineItem
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders a timeline item.
   *
   * @param {object}  ngModel The timeline item
   * @param {string}  contextSenders Optionally, the ID of the context senders, that determine if a post is displayed as a share.
   * @param {boolean} isNew Show the new ribbon
   */
  function timelineItem() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/modules/timeline/components/timeline-item/timeline-item.html',
      scope: {},
      require: 'ngModel',
      bindToController: {
        ngModel: '=',
        contextSenders: '@',
        isNew: '<'
      },
      controller: 'TimelineItemDirectiveController',
      controllerAs: 'tlItem'
    };
  }

  function TimelineItemDirectiveController($scope, authService, backendUrlService, coyoEndpoints, reportService,
                                           timelineEditModalService) {
    var vm = this;

    vm.backendUrl = backendUrlService.getUrl();
    vm.preview = coyoEndpoints.timeline.preview;
    vm.tplPrefix = 'app/modules/timeline/components/timeline-item/types/';
    vm.showOriginalAuthor = false;
    vm.originalAuthor = null;

    vm.edit = edit;
    vm.report = _.partial(reportService.report, vm.ngModel.id, vm.ngModel.typeName);
    vm.remove = remove;
    vm.markAsRead = markAsRead;
    vm.toggleOriginalAuthor = toggleOriginalAuthor;

    function remove() {
      vm.ngModel.remove();
    }

    function edit() {
      timelineEditModalService.open(vm.ngModel).then(function (updatedItem) {
        angular.extend(vm.ngModel, updatedItem);
      });
    }

    function markAsRead() {
      vm.loadingMarkAsRead = true;
      vm.ngModel.markAsRead().then(function (item) {
        vm.ngModel.unread = item.unread;
      }).finally(function () {
        vm.loadingMarkAsRead = false;
      });
    }

    function toggleOriginalAuthor() {
      if (!vm.originalAuthor) {
        vm.ngModel.getOriginalAuthor().then(function (originalAuthor) {
          vm.originalAuthor = originalAuthor;
          vm.showOriginalAuthor = !vm.showOriginalAuthor;
        });
      } else {
        vm.showOriginalAuthor = !vm.showOriginalAuthor;
      }
    }

    (function _init() {
      authService.getUser().then(function (user) {
        vm.currentUser = user;
      });

      // calculate active share for header display
      $scope.$watch(function () {
        return vm.ngModel.shares;
      }, function (newVal) {
        vm.share = _.chain(newVal).filter(function (share) {
          return share.recipient && _.includes(vm.contextSenders, share.recipient.id);
        }).orderBy(['created'], ['desc']).head().value();
      });

      // calculate recipients for header display
      $scope.$watch(function () {
        return vm.ngModel.recipients;
      }, function (newVal) {
        vm.recipients = _.chain(newVal)
            .reject({id: vm.ngModel.author.id})
            .slice(0, 3).value();
      });

      // watch global permissions
      var unsubscribeReportsFn = authService.onGlobalPermissions('CREATE_REPORTS', function (canCreateReports) {
        vm.canCreateReports = canCreateReports;
      });
      var unsubscribePostsFn = authService.onGlobalPermissions('DELETE_TIMELINE_ITEM', function (canDeletePosts) {
        vm.canDeletePost = canDeletePosts && vm.ngModel._permissions.delete;
      });
      $scope.$on('$destroy', unsubscribeReportsFn);
      $scope.$on('$destroy', unsubscribePostsFn);
    })();
  }

})(angular);
