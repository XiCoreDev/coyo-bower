(function (angular) {
  'use strict';

  angular
      .module('coyo.timeline')
      .directive('coyoTimelineShare', timelineShare)
      .controller('TimelineShareDirectiveController', TimelineShareDirectiveController);

  /**
   * @ngdoc directive
   * @name coyo.timeline.coyoTimelineShare:coyoTimelineShare
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders a timeline share.
   *
   * @param {object} ngModel The timeline share
   */
  function timelineShare() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/modules/timeline/components/timeline-share/timeline-share.html',
      scope: {},
      require: 'ngModel',
      bindToController: {
        ngModel: '<'
      },
      controller: 'TimelineShareDirectiveController',
      controllerAs: 'tlShare'
    };
  }

  function TimelineShareDirectiveController(authService) {
    var vm = this;

    // load current user and determine headline
    authService.getUser().then(function (user) {
      vm.currentUser = user;

      var authorPart = 'NONE';
      if (vm.ngModel.author) {
        authorPart = vm.ngModel.author.id === user.id ? 'YOU' : 'OTHER';
      }

      var recipientPart = 'NONE';
      if (vm.ngModel.recipient) {
        recipientPart = vm.ngModel.recipient.id === user.id ? 'YOU' : 'OTHER';
      }

      vm.headline = 'MODULE.TIMELINE.SHARE.HEADLINE.' + authorPart + '_WITH_' + recipientPart;
      vm.headlineSuffix = vm.headline + '.SUFFIX';
    });
  }

})(angular);
