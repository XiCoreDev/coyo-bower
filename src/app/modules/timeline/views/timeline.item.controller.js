(function (angular) {
  'use strict';

  angular
      .module('coyo.timeline')
      .controller('TimelineItemController', TimelineItemController);

  /**
   * Controller for showing a single timeline item.
   */
  function TimelineItemController($state, $scope, socketService, msmNotification, item) {
    var vm = this;
    vm.item = item;

    // subscribe to socket events
    var unsubscribeFn = socketService.subscribe('/topic/timeline.item.' + item.id + '.deleted', onRemove);
    $scope.$on('$destroy', unsubscribeFn);

    function onRemove() {
      msmNotification.warning('MODULE.TIMELINE.ITEM.DELETED');
      $state.go('main');
    }
  }

})(angular);
