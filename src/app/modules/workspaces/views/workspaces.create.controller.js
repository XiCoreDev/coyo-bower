(function (angular) {
  'use strict';

  angular
      .module('coyo.workspaces')
      .controller('WorkspacesCreateController', WorkspacesCreateController);

  function WorkspacesCreateController($q, $state, WorkspaceModel, msmNotification) {
    var vm = this;

    vm.back = back;
    vm.next = next;

    function back() {
      vm.wizard.active = Math.max(0, vm.wizard.active - 1);
    }

    function next(form) {
      if (form && form.$valid) {
        if (vm.wizard.active < vm.wizard.states.length - 1) {
          return $q.resolve(vm.wizard.active++);
        } else {
          vm.workspace.categoryIds = _.map(vm.workspace.categories, 'id');
          return vm.workspace.create().then(function (workspace) {
            $state.go('main.workspace.show', {idOrSlug: workspace.slug});
            msmNotification.success('MODULE.WORKSPACES.CREATE.SUCCESS');
          });
        }
      }
      return $q.reject();
    }

    (function _init() {
      vm.wizard = {
        states: ['MODULE.WORKSPACES.CREATE.GENERAL', 'MODULE.WORKSPACES.CREATE.ACCESS'],
        active: 0
      };

      vm.workspace = new WorkspaceModel({
        visibility: 'PUBLIC',
        adminIds: [],
        memberIds: [],
        adminGroupIds: [],
        memberGroupIds: []
      });
    })();
  }

})(angular);
