(function (angular) {
  'use strict';

  angular
      .module('coyo.workspaces')
      .controller('WorkspacesListController', WorkspacesListController);

  function WorkspacesListController($rootScope, $scope, $q, $sessionStorage, $state, $stateParams, $timeout, WorkspaceModel, WorkspaceCategoryModel, Pageable, currentUser, categories, authService, msmModal, workspacesConfig) {
    var vm = this;

    vm.currentUser = currentUser;
    vm.treeOptions = _buildTreeOptions();
    vm.categories = categories;

    vm.showWorkspaceActions = showWorkspaceActions;
    vm.search = search;
    vm.getTotalCount = getTotalCount;
    vm.isCategoryActive = isCategoryActive;
    vm.toggleCategory = toggleCategory;
    vm.addCategory = addCategory;
    vm.editCategory = editCategory;
    vm.saveCategory = saveCategory;
    vm.onCategoryKeypress = onCategoryKeypress;
    vm.onCategoryBlur = onCategoryBlur;
    vm.deleteCategory = deleteCategory;

    function showWorkspaceActions(currentWorkspace) {
      return ((currentWorkspace.visibility === 'PUBLIC' || currentWorkspace.visibility === 'PROTECTED') || (currentWorkspace.visibility === 'PRIVATE' && currentWorkspace.membershipStatus !== 'NONE'));
    }

    function _loadWorkspaces() {
      if (vm.loading) {
        return;
      }

      // write params to URL
      $state.transitionTo('main.workspace', _.omitBy({
        term: _.get(vm.query, 'term', ''),
        'categories[]': _.get(vm.query, 'filters.categories', [])
      }, _.isEmpty), {notify: false});

      // perform search
      $sessionStorage.workspaceQuery = vm.query;
      vm.loading = true;

      var term = vm.query.term;
      var sort = term ? ['_score,DESC', 'displayName.sort'] : 'displayName.sort';
      var pageable = new Pageable(0, workspacesConfig.list.paging.pageSize, sort);
      var filters = vm.query.filters;
      var fields = ['displayName'];
      var aggregations = {categories: 0};
      WorkspaceModel.searchWithFilter(term, pageable, filters, fields, aggregations, true).then(function (page) {
        vm.currentPage = page;
        _.forEach(vm.categories, function (category) {
          var data = _.find(page.aggregations.categories, {key: category.id});
          category.count = _.get(data, 'count', 0);
        });
        var missingData = _.find(page.aggregations.categories, {key: 'N/A'});
        vm.missingCount = _.get(missingData, 'count', null);
      }).finally(function () {
        vm.loading = false;
      });
    }

    function search(searchTerm) {
      if (!vm.editingCategory) {
        vm.query.term = searchTerm;
        vm.query.filters = {categories: []};
        _loadWorkspaces();
      }
    }

    /* ===== Categories ===== */

    vm.editingCategory = null;
    vm.savingCategory = false;

    function getTotalCount() {
      return _.sumBy(vm.categories, 'count') + (vm.missingCount || 0);
    }

    function isCategoryActive(category) {
      var active = _.get(vm.query, 'filters.categories', []);
      return category ? active.indexOf(category.id) !== -1 : active.length === 0;
    }

    function toggleCategory(category) {
      if (!vm.editingCategory) {
        vm.query.filters = {categories: category ? [category.id] : []};
        _loadWorkspaces();
      }
    }

    function addCategory($event) {
      if (vm.editingCategory || vm.savingCategory) {
        return;
      }
      vm.editingCategory = new WorkspaceCategoryModel();
      vm.categories.push(vm.editingCategory);

      $timeout(function () {
        var target = angular.element($event.currentTarget);
        target.parent().find('input').select();
      });
    }

    function editCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (vm.editingCategory || vm.savingCategory) {
        return;
      }
      vm.editingCategory = angular.copy(category);

      $timeout(function () {
        var target = angular.element($event.currentTarget);
        target.parent().parent().find('input').select();
      });
    }

    function saveCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (!category.name || vm.savingCategory) {
        return $q.reject();
      }

      vm.savingCategory = true;
      return category.save().then(function (result) {
        vm.categories.splice(_.findIndex(vm.categories, {id: category.id}), 1, result);
      }).finally(function () {
        vm.savingCategory = false;
        vm.editingCategory = null;
      });
    }

    function onCategoryKeypress(category, $event) {
      if ($event.keyCode === 13) {
        saveCategory(category, $event);
      }
    }

    function onCategoryBlur(category, $event) {
      if (!vm.editingCategory || !category) {
        return; // not in edit mode
      } else if ($event) {
        var currentTarget = angular.element($event.currentTarget);
        var relatedTarget = angular.element($event.relatedTarget);
        if (currentTarget[0] === relatedTarget.find('input')[0]) {
          return; // clicked on current filter
        }
      }

      $timeout(function () {
        if (category.isNew()) {
          vm.categories.pop();
        } else {
          category.name = vm.editingCategory.name;
        }
        vm.editingCategory = null;
      });
    }

    function deleteCategory(category, $event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();

      if (vm.editingCategory || vm.savingCategory) {
        return $q.reject();
      }

      return msmModal.confirm({
        title: 'WORKSPACE.CATEGORY.DELETE.MODAL.TITLE',
        text: 'WORKSPACE.CATEGORY.DELETE.MODAL.TEXT',
        translationContext: {category: category.name},
        close: {title: 'YES'},
        dismiss: {title: 'NO'}
      }).result.then(function () {
        vm.savingCategory = true;
        return category.delete().then(function (result) {
          _.remove(vm.categories, {id: category.id});
          if (category.id === _.get(vm.query, 'filters.categories[0]', null)) {
            vm.query.filters = {categories: []};
            _loadWorkspaces();
          }
          return result;
        });
      }).finally(function () {
        vm.savingCategory = false;
      });
    }

    /* ==================== */

    function _buildTreeOptions() {
      return {
        dropped: function (event) {
          // persist new sort order
          if (event.source.index !== event.dest.index) {
            WorkspaceCategoryModel.order(_.map(vm.categories, 'id'));
          }
        }
      };
    }

    (function _init() {
      // extract search from URL / storage
      vm.query = $sessionStorage.workspaceQuery || {};
      if ($stateParams.term || $stateParams['categories[]']) {
        angular.extend(vm.query, {
          term: $stateParams.term,
          filters: {categories: $stateParams['categories[]']}
        });
      }

      // register ESC callback
      var unsubscribeEscFn = $rootScope.$on('keyup:esc', function () {
        if (vm.editingCategory) {
          var category = _.find(vm.categories, function (c) {
            return vm.editingCategory.isNew() ? c.isNew() : vm.editingCategory.id === c.id;
          });
          onCategoryBlur(category);
        }
      });

      // register permission callback
      var unsubscribePermissionFn = authService.onGlobalPermissions('MANAGE_WORKSPACE_CATEGORIES', function (canManage) {
        vm.canManageWorkspaceCategories = canManage;
      });

      $timeout(function () {
        _loadWorkspaces();
      });

      // unregister callbacks on $scope death
      $scope.$on('$destroy', function () {
        unsubscribeEscFn();
        unsubscribePermissionFn();
      });
    })();
  }

})(angular);
