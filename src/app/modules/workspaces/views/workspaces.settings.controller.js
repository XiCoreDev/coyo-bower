(function (angular) {
  'use strict';

  angular
      .module('coyo.workspaces')
      .controller('WorkspaceSettingsController', WorkspaceSettingsController);

  function WorkspaceSettingsController($state, workspace, WorkspaceModel, msmNotification, msmModal) {
    var vm = this;

    vm.save = save;
    vm.delete = deleteWorkspace;

    function save() {
      vm.workspace.categoryIds = _.map(vm.workspace.categories, 'id');
      return vm.workspace.update().then(function (workspace) {
        msmNotification.success('MODULE.WORKSPACES.EDIT.SUCCESS');

        if ($state.is('main.workspace.show.settings')) {
          $state.go('main.workspace.show', {idOrSlug: workspace.slug}, {reload: 'main.workspace.show'});
        }
      });
    }

    function deleteWorkspace() {
      msmModal.confirm({title: 'WORKSPACE.DELETE', text: 'WORKSPACE.DELETE.CONFIRM'}).result.then(function () {
        vm.workspace.delete().then(function () {
          $state.go('main.workspace');
          msmNotification.success('WORKSPACE.DELETE.SUCCESS');
        });
      });
    }

    (function _init() {
      vm.baseUrl = $state.href('main.workspace', {}) + '/';
      vm.oldName = workspace.displayName;
      vm.oldSlug = workspace.slug;

      vm.workspace = new WorkspaceModel(workspace);
      _.forEach(vm.workspace.categories, function (category) {
        category.displayName = category.name;
      });
    })();
  }

})(angular);
