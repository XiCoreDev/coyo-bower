(function (angular) {
  'use strict';

  angular
      .module('coyo.workspaces')
      .controller('WorkspaceMembersController', WorkspaceMembersController);

  function WorkspaceMembersController($state, userChooserModalService, workspace) {
    var vm = this;
    var selfState = 'main.workspace.show.members.invited';

    vm.workspace = workspace;
    vm.inviteMembers = inviteMembers;
    vm.inviteAdmins = inviteAdmins;

    function inviteMembers() {
      userChooserModalService.open({}, {}).then(function (selected) {
        workspace.inviteMembers(selected).then(function () {
          $state.go(selfState, {
            idOrSlug: workspace.slug
          }, {
            reload: selfState
          });
        });
      });
    }

    function inviteAdmins() {
      userChooserModalService.open({}, {}).then(function (selected) {
        workspace.inviteAdmins(selected).then(function () {
          $state.go(selfState, {
            idOrSlug: workspace.slug
          }, {
            reload: selfState
          });
        });
      });
    }
  }

})(angular);
