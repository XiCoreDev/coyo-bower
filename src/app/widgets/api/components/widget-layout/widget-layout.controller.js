(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.api')
      .controller('WidgetLayoutController', WidgetLayoutController);

  function WidgetLayoutController($q, $scope, $timeout, msmModal, widgetEditService, WidgetLayoutModel, msmUtil) {
    var vm = this;

    vm.renderStyle = vm.renderStyle || 'plain';
    vm.loading = true;
    vm.loadingPromises = [];
    vm.editMode = false;
    vm.canManage = angular.isDefined(vm.canManage) ? vm.canManage : true;
    vm.globalEvents = angular.isDefined(vm.globalEvents) ? vm.globalEvents : true;

    vm.addRow = addRow;
    vm.removeRow = removeRow;
    vm.isRowVisible = isRowVisible;

    var createMode = vm.createMode; // preserve initial status as it might change before the save event is handled

    var layoutResponse;
    if (createMode) {
      layoutResponse = $q.resolve(new WidgetLayoutModel({
        settings: {rows: [{slots: [{cols: 12}]}]}
      }));
    } else {
      layoutResponse = new WidgetLayoutModel({name: vm.name}).get();
    }

    layoutResponse.then(function (layout) {
      vm.layout = layout;

      angular.forEach(vm.layout.settings.rows, function (row) {
        if (!row.name) {
          row.name = msmUtil.uuid();
        }
      });

      $scope.$emit('widget-layout:loaded');
    }).catch(function (error) {
      $scope.$emit('widget-layout:loadError', error);
    }).finally(function () {
      $timeout(function () {
        $q.all(vm.loadingPromises).finally(function () {
          vm.loading = false;
        });
      });
    });

    var unsubscribeEditFn = $scope.$on('widget-slot:edit', function (event, isGlobal) {
      if (!vm.globalEvents && isGlobal) {
        return;
      }
      vm.layout.snapshot();
      vm.editMode = true;
    });

    var unsubscribeCancelFn = $scope.$on('widget-slot:cancel', function (event, isGlobal) {
      if (!vm.globalEvents && isGlobal) {
        return;
      }
      vm.layout.rollback();
      vm.editMode = false;
    });

    var unsubscribeSaveFn = $scope.$on('widget-slot:save', function (event, promises, isGlobal) {
      if (!vm.globalEvents && isGlobal) {
        return;
      }
      angular.extend(vm.layout, {
        name: vm.name,
        parentId: angular.isDefined(vm.parent) ? vm.parent.id : null,
        parentType: angular.isDefined(vm.parent) ? vm.parent.typeName : null
      });
      var index = 0;
      _.forEach(vm.layout.settings.rows, function (row) {
        _.forEach(row.slots, function (slot) {
          if (!slot.name) {
            slot.name = vm.name + '-' + index++;
          }
        });
      });

      if (createMode || vm.copyMode) {
        vm.layout.create();
      } else {
        vm.layout.update();
      }

      vm.editMode = false;
    });

    if (vm.canManage && vm.globalEvents) {
      widgetEditService.register(vm.name, !vm.parent);
    }

    $scope.$on('$destroy', function () {
      widgetEditService.deregister(vm.name);
      unsubscribeEditFn();
      unsubscribeCancelFn();
      unsubscribeSaveFn();
    });

    // ------------------------------------------------------------------------------------------

    function addRow(index) {
      msmModal.open({
        controller: 'WidgetLayoutRowChooserController',
        templateUrl: 'app/widgets/api/components/widget-layout/widget-layout-row-chooser-modal.html',
        resolve: {
          layout: function () {
            return vm.layout;
          },
          index: function () {
            return index;
          }
        }
      });
    }

    function removeRow(index) {
      msmModal.confirm({
        title: 'WIDGETS.LAYOUT.ROW.DELETE',
        text: 'WIDGETS.LAYOUT.ROW.DELETE.CONFIRM',
        close: {title: 'YES'},
        dismiss: {title: 'NO'}
      }).result.then(function () {
        vm.layout.settings.rows[index].$deleted = true;
      });
    }

    function isRowVisible(row) {
      return row.$deleted !== true;
    }
  }

})(angular);
