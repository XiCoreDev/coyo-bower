(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.blog')
      .controller('ButtonWidgetSettingsController', ButtonWidgetSettingsController);

  function ButtonWidgetSettingsController($scope, buttonWidgetStyleOptions, SettingsModel) {
    var vm = this;
    vm.model = $scope.model;
    vm.options = buttonWidgetStyleOptions;
    vm.validate = validate;

    if (!vm.model.settings._button) {
      vm.model.settings._button = vm.options[0];
    } else {
      vm.urlValid = true;
      vm.validUrl = vm.model.settings._url;
    }

    function validate() {
      var val = vm.model.settings._url;
      vm.urlValid = !_.isEmpty(val) || (val && val.match(vm.linkPattern) !== null);
      if (vm.urlValid) {
        vm.validUrl = vm.model.settings._url;
      }
    }

    SettingsModel.retrieveByKey('linkPattern').then(function (response) {
      vm.linkPattern = response;
    });
  }

})(angular);
