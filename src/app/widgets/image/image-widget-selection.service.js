(function (angular) {
  'use strict';

  angular.module('coyo.widgets.image')
      .factory('imageWidgetSelectionService', imageWidgetSelectionService);

  /**
   * @ngdoc service
   * @name coyo.widgets.image.imageWidgetSelectionService
   *
   * @description
   * Internal service to handle image selection in the image widget.
   *
   * @requires coyo.domain.SenderModel
   * @requires commons.ui.fileLibraryModalService
   */
  function imageWidgetSelectionService(SenderModel, fileLibraryModalService, $q) {

    return {
      selectImage: selectImage
    };

    /**
     * @ngdoc overview
     * @name coyo.widgets.image.imageWidgetSelectionService#selectImage
     * @memberOf coyo.widgets.image.imageWidgetSelectionService
     *
     * @description
     * Select an image via the file library modal and assign the (single) selected image to the widget model.
     * If the widget is located in the context of a sender this sender will be preselected.
     *
     * @param {object} model
     * The widget domain model.
     */
    function selectImage(model) {
      var senderIdOrSlug = SenderModel.getCurrentIdOrSlug();
      if (angular.isDefined(senderIdOrSlug)) {
        return SenderModel.getWithPermissions(senderIdOrSlug, {}, ['createFile']).then(function (sender) {
          return openFileLibrary(model, sender);
        });
      } else {
        return $q.resolve(openFileLibrary(model));
      }
    }

    function openFileLibrary(model, sender) {
      var options = {
        uploadMultiple: false,
        selectMode: 'single',
        filterContentType: 'image'
      };
      var cropSettings = {
        cropImage: true
      };
      return fileLibraryModalService.open(sender, options, cropSettings).then(function (selectedFiles) {
        var image = selectedFiles;
        model.settings._image = {
          id: image.id,
          senderId: image.senderId
        };
      });
    }
  }

})(angular);
