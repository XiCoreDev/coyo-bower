(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.widgets
   *
   * @description
   * This module contains all widgets and the API used by widgets.
   */
  angular
      .module('coyo.widgets', [
        'coyo.widgets.blog',
        'coyo.widgets.bookmarking',
        'coyo.widgets.wiki',
        'coyo.widgets.callout',
        'coyo.widgets.divider',
        'coyo.widgets.headline',
        'coyo.widgets.image',
        'coyo.widgets.personal-timeline',
        'coyo.widgets.subscriptions',
        'coyo.widgets.rte',
        'coyo.widgets.text',
        'coyo.widgets.useronline',
        'coyo.widgets.callout',
        'coyo.widgets.blogarticle',
        'coyo.widgets.button',
        'coyo.widgets.html',
        'coyo.widgets.completeprofile',
        'coyo.widgets.userprofile',
        'coyo.widgets.welcome',
        'coyo.widgets.downloads',
        'coyo.widgets.suggestpages',
        'coyo.widgets.wikiarticle',
        'coyo.widgets.singlefile',
        'coyo.widgets.teaser',
        'coyo.widgets.iframe',
        'coyo.widgets.poll',
        'coyo.widgets.hashtag'
      ]);

})(angular);
